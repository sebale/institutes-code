<?php
require_once($CFG->libdir. "/../course/renderer.php");
class theme_institutes_core_course_renderer extends core_course_renderer{
	
    /**
     * Renders HTML to display one course module in a course section
     *
     * This includes link, content, availability, completion info and additional information
     * that module type wants to display (i.e. number of unread forum posts)
     *
     * This function calls:
     * {@link core_course_renderer::course_section_cm_name()}
     * {@link core_course_renderer::course_section_cm_text()}
     * {@link core_course_renderer::course_section_cm_availability()}
     * {@link core_course_renderer::course_section_cm_completion()}
     * {@link course_get_cm_edit_actions()}
     * {@link core_course_renderer::course_section_cm_edit_actions()}
     *
     * @param stdClass $course
     * @param completion_info $completioninfo
     * @param cm_info $mod
     * @param int|null $sectionreturn
     * @param array $displayoptions
     * @return string
     */
    public function course_section_cm($course, &$completioninfo, cm_info $mod, $sectionreturn, $displayoptions = array()) {
        $output = '';
        // We return empty string (because course module will not be displayed at all)
        // if:
        // 1) The activity is not visible to users
        // and
        // 2) The 'availableinfo' is empty, i.e. the activity was
        //     hidden in a way that leaves no info, such as using the
        //     eye icon.
        if (!$mod->uservisible && empty($mod->availableinfo)) {
            return $output;
        }
        
        if ($completioninfo === null) {
            $completioninfo = new completion_info($course);
        }

        $indentclasses = 'mod-indent';
        if (!empty($mod->indent)) {
            $indentclasses .= ' mod-indent-'.$mod->indent;
            if ($mod->indent > 15) {
                $indentclasses .= ' mod-indent-huge';
            }
        }
        
        if ($this->page->user_is_editing()) {
        
            $output .= html_writer::start_tag('div');

            if ($this->page->user_is_editing()) {
                $output .= course_get_cm_move($mod, $sectionreturn);
            }

            $output .= html_writer::start_tag('div', array('class' => 'mod-indent-outer'));

            // This div is used to indent the content.
            $output .= html_writer::div('', $indentclasses);

            // Start a wrapper for the actual content to keep the indentation consistent
            $output .= html_writer::start_tag('div');

            // Display the link to the module (or do nothing if module has no url)
            $cmname = $this->course_section_cm_name($mod, $displayoptions);

            if (!empty($cmname)) {
                // Start the div for the activity title, excluding the edit icons.
                $output .= html_writer::start_tag('div', array('class' => 'activityinstance'));
                $output .= $cmname;

                // Module can put text after the link (e.g. forum unread)
                $output .= $mod->afterlink;

                // Closing the tag which contains everything but edit icons. Content part of the module should not be part of this.
                $output .= html_writer::end_tag('div'); // .activityinstance
            }

            // If there is content but NO link (eg label), then display the
            // content here (BEFORE any icons). In this case cons must be
            // displayed after the content so that it makes more sense visually
            // and for accessibility reasons, e.g. if you have a one-line label
            // it should work similarly (at least in terms of ordering) to an
            // activity.
            $contentpart = $this->course_section_cm_text($mod, $displayoptions);
            $url = $mod->url;
            if (empty($url)) {
                $output .= $contentpart;
            }

            $modicons = '';
            if ($this->page->user_is_editing()) {
                $editactions = course_get_cm_edit_actions($mod, $mod->indent, $sectionreturn);
                $modicons .= ' '. $this->course_section_cm_edit_actions($editactions, $mod, $displayoptions);
                $modicons .= $mod->afterediticons;
            }

            $modicons .= $this->course_section_cm_completion($course, $completioninfo, $mod, $displayoptions);

            if (!empty($modicons)) {
                $output .= html_writer::span($modicons, 'actions');
            }

            // If there is content AND a link, then display the content here
            // (AFTER any icons). Otherwise it was displayed before
            if (!empty($url)) {
                $output .= $contentpart;
            }

            // show availability info (if module is not available)
            $output .= $this->course_section_cm_availability($mod, $displayoptions);

            $output .= html_writer::end_tag('div'); // $indentclasses

            // End of indentation div.
            $output .= html_writer::end_tag('div');

            $output .= html_writer::end_tag('div');
        
        } else {
            
            $output .= html_writer::start_tag('div', array('class'=>'activity-box clearfix'));

            $output .= html_writer::start_tag('div', array('class' => 'mod-indent-outer'));

            // This div is used to indent the content.
            $output .= html_writer::div('', $indentclasses);

            // Start a wrapper for the actual content to keep the indentation consistent
            $output .= html_writer::start_tag('div');

            // Display the link to the module (or do nothing if module has no url)
            $cmname = $this->course_section_cm_name($mod, $displayoptions);

            if (!empty($cmname)) {
                // Start the div for the activity title, excluding the edit icons.
                $output .= html_writer::start_tag('div', array('class' => 'activityinstance'));
                $output .= $cmname;

                // Module can put text after the link (e.g. forum unread)
                $output .= $mod->afterlink;
                
                // Closing the tag which contains everything but edit icons. Content part of the module should not be part of this.
                $output .= html_writer::end_tag('div'); // .activityinstance
            }
            
            $output .= html_writer::end_tag('div'); // $indentclasses

            // End of indentation div.
            $output .= html_writer::end_tag('div');
            $output .= html_writer::start_tag('div', array('class'=>'activity-buttons'));

            $output .= '<table class="activity-table">';
            $output .= '<tr>';

            if ($mod->modname != 'label') {
                $viewbtn = 'View ' . get_string('pluginname', 'mod_' . $mod->modname);
                switch ($mod->modname) {
                    case 'quiz':
                        $viewbtn = 'Take ' . get_string('pluginname', 'mod_' . $mod->modname);
                        break;
                    case 'scorm':
                        $viewbtn = 'View module';
                        break;
                    case 'lti':
                        $viewbtn = 'View Link';
                        break;
                }

                $link = '';
                if ($mod->uservisible and $mod->url) {
                    $onclick = htmlspecialchars_decode($mod->onclick, ENT_QUOTES);
                    $link = html_writer::link($mod->url, $viewbtn, array('class' => 'btn', 'onclick' => $onclick));
                } else {
                    $link = html_writer::link('javascript:void(0)', $viewbtn, array('class' => 'btn btn-disabled'));
                }
                $output .= '<td>' . $link . '</td>';
            }

            $output .= '<td>' . $this->course_section_cm_completion_button($course, $completioninfo, $mod, $displayoptions) . '</td>';
            $output .= '<td>' . $this->course_section_cm_mark_complete($course, $mod, $completioninfo) . '</td>';

            $output .= '</tr>';
            $output .= '</table>';

            /*if ($mod->modname == 'quiz'){
                $output .= $this->course_section_cm_quiz_button($course, $completioninfo, $mod, $displayoptions);
            }*/

            $output .= html_writer::end_tag('div');
            
            // If there is content but NO link (eg label), then display the
            // content here (BEFORE any icons). In this case cons must be
            // displayed after the content so that it makes more sense visually
            // and for accessibility reasons, e.g. if you have a one-line label
            // it should work similarly (at least in terms of ordering) to an
            // activity.
            $contentpart = $this->course_section_cm_text($mod, $displayoptions);
            
            if (!empty($contentpart)){
                if (empty($url)) {
                    $output .= html_writer::tag('div', $contentpart, array('class'=>'mod-text'));
                } else {
                    $output .= html_writer::tag('div', $contentpart, array('class'=>'mod-description'));
                }
            }

            // show availability info (if module is not available)
            $output .= $this->course_section_cm_availability($mod, $displayoptions);

            $output .= html_writer::end_tag('div');
        }
        return $output;
    }

    /**
     * @param $course
     * @param $mod
     * @param $completioninfo
     * @return string
     */
    public function course_section_cm_mark_complete($course, $mod, $completioninfo) {

        $o = '';
        if (!$mod->uservisible && empty($mod->availableinfo)) {
            return $o;
        }

        $completion = $completioninfo->is_enabled($mod);
        if ($completion == COMPLETION_TRACKING_NONE) {
            return $o;
        }

        $completiondata = $completioninfo->get_data($mod, true);
        if ($completion == COMPLETION_TRACKING_MANUAL) {

            $newstate = $completiondata->completionstate == COMPLETION_COMPLETE
                    ? COMPLETION_INCOMPLETE
                    : COMPLETION_COMPLETE;

            $title = ($completiondata->completionstate == COMPLETION_COMPLETE) ? 'Mark incomplete' : 'Mark complete';
            $newtitle = ($completiondata->completionstate != COMPLETION_COMPLETE) ? 'Mark incomplete' : 'Mark complete';
            //$btnclass = ($completiondata->completionstate == COMPLETION_COMPLETE) ? 'btn-warning' : 'btn-success';
            $btnclass = ($completiondata->completionstate == COMPLETION_COMPLETE) ? 'btn-success' : 'btn-warning';

            $o .= html_writer::start_tag('form', array('method' => 'post',
                'action'=> new moodle_url('/course/togglecompletion.php'),
                'class' => 'togglecompletion',
                'id'    => 'toggle_module_completion_' . $mod->id));
            $o .= html_writer::empty_tag('input', array(
                'type' => 'hidden', 'name' => 'id', 'value' => $mod->id));
            $o .= html_writer::empty_tag('input', array(
                'type' => 'hidden', 'name' => 'sesskey', 'value' => sesskey()));
            $o .= html_writer::empty_tag('input', array(
                'type' => 'hidden', 'name' => 'modulename', 'value' => $mod->name));
            $o .= html_writer::empty_tag('input', array(
                'type' => 'hidden', 'name' => 'completionstate', 'value' => $newstate));
            $o .= html_writer::empty_tag('input', array(
                'type' => 'hidden', 'name' => 'state', 'value' => $completiondata->completionstate));
            $o .= html_writer::empty_tag('input', array(
                'type' => 'hidden', 'name' => 'title', 'value' => $title));
            $o .= html_writer::empty_tag('input', array(
                'type' => 'hidden', 'name' => 'newtitle', 'value' => $newtitle));
            $o .= html_writer::tag('button', $title, array('type'=>'button', 'onclick'=>'_toggleModuleCompletion($(this).parent()); return false;', 'class'=>'btn btn-inverse ' . $btnclass));
            $o .= html_writer::end_tag('form');
        }

        return $o;
    }
    
    /**
     * Renders html for completion box on course page
     *
     * If completion is disabled, returns empty string
     * If completion is automatic, returns an icon of the current completion state
     * If completion is manual, returns a form (with an icon inside) that allows user to
     * toggle completion
     *
     * @param stdClass $course course object
     * @param completion_info $completioninfo completion info for the course, it is recommended
     *     to fetch once for all modules in course/section for performance
     * @param cm_info $mod module to show completion for
     * @param array $displayoptions display options, not used in core
     * @return string
     */
    public function course_section_cm_completion_button($course, &$completioninfo, cm_info $mod, $displayoptions = array()) {
        global $CFG;
        $output = '';
        if (!empty($displayoptions['hidecompletion']) || !isloggedin() || isguestuser() || !$mod->uservisible) {
            return $output;
        }
        if ($completioninfo === null) {
            $completioninfo = new completion_info($course);
        }
        $completion = $completioninfo->is_enabled($mod);
        if ($completion == COMPLETION_TRACKING_NONE) {
            return $output;
        }

        $completiondata = $completioninfo->get_data($mod, true);
        $completed = false;
        $completionclass = ' btn-info';
        $completiontext = 'Not Started';
        
        if ($completion == COMPLETION_TRACKING_MANUAL) {
            switch($completiondata->completionstate) {
                case COMPLETION_COMPLETE:
                    $completed = true; break;
            }
        } else { // Automatic
            switch($completiondata->completionstate) {
                case COMPLETION_COMPLETE:
                    $completed = true; break;
                case COMPLETION_COMPLETE_PASS:
                    $completed = true; break;
            }
        }
        
        if ($completiondata->viewed and !$completed){
            $completionclass = ' btn-warning';
            $completiontext = 'In Progress';
        } elseif ($completed) {
            $completionclass = ' btn-success';
            $completiontext = 'Complete';
        }

        $output .= html_writer::tag('span', '', ['class' => 'btn-divider']) . html_writer::tag('span', $completiontext, array('class' => 'btn-completion btn'.$completionclass));
        return $output;
    }
    
    public function course_section_cm_quiz_button($course, &$completioninfo, cm_info $mod, $displayoptions = array()) {
        global $CFG;
        $output = '';
        if (!$mod->uservisible) return $output;
        
        $divider = '<span class="btn-divider"></span>';
        
        $output .= $divider.html_writer::link('#', 'AVG: N/A', array('class' => 'btn btn-empty'));
        $output .= $divider.html_writer::link('#', 'Attempts', array('class' => 'btn btn-empty'));
        
        return $output;
    }
	  
}

?>