<?php
    global $OUTPUT, $CFG, $USER;

    $alerts = ($USER->id) ? get_user_alerts() : [];
?>

<header role="banner" class="navbar navbar-fixed-top<?php echo $html->navbarclass ?> moodle-has-zindex clearfix">
    <div class="header-logo">

        <div class="mobile-nav"></div>

        <?php if ($PAGE->course->id > 1) : ?>
            <?php echo theme_institutes_get_course_header(); ?>
        <?php else: ?>
            <a href="<?php echo $CFG->wwwroot;?>"><div class="logo"></div></a>
            <span class="header_course_name"> <?php echo $PAGE->course->fullname ?> </span>
        <?php endif; ?>
    </div>
    <?php if (isloggedin() and theme_has_editing_capability()) : ?>
        <div class="header-navbar">
            <div class="container-fluid header-search-panel clearfix">
                <?php echo $OUTPUT->search_box(); ?>
                <div class="header-notifications-menu">
                    <?php $actions_buttons = $OUTPUT->page_heading_button(); ?>
                    <ul class="clearfix<?php echo (empty($actions_buttons) ? ' two-items' : ''); ?>">
                        <li>
                            <a href="<?php echo $CFG->wwwroot; ?>" title="<?php echo get_string('alerts', 'theme_institutes'); ?>">
                                <span class="menu-icon icon-alert"></span>
                                <span class="menu-text"><?php echo get_string('alerts', 'theme_institutes'); ?></span>

                                <?php if($alerts['count']): ?>
                                    <i class="alert notification"><?= $alerts['count'] ?></i>
                                <?php endif; ?>
                            </a>
                            <?= $alerts['list'] ?>
                        </li>
                        <li>
                            <a href="<?php echo $CFG->wwwroot; ?>/message/index.php">
                                <span class="menu-icon icon-messages" title="<?php echo get_string('messages', 'theme_institutes'); ?>"></span>
                                <span class="menu-text"><?php echo get_string('messages', 'theme_institutes'); ?></span>
                                <!--<i class="alert messages"></i>-->
                            </a>
                        </li>

                        <?php if (!empty($actions_buttons)) : ?>
                            <li class="moodle-editing-btn">
                                <a href="javascript:void(0);" title="<?php echo get_string('actions', 'theme_institutes'); ?>"><i class="fa fa-gear"></i> <span class="menu-text"><?php echo get_string('actions', 'theme_institutes'); ?></span></a>
                                <div class="actions-menu">    
                                    <?php echo $OUTPUT->page_heading_button(); ?>
                                </div>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
                <div class="nav-collapse collapse">
                    <?php echo $OUTPUT->custom_menu(); ?>
                    <ul class="nav pull-right">
                        <li><?php echo $OUTPUT->page_heading_menu(); ?></li>
                    </ul>
                </div>
            </div>
            <div class="container-fluid header-path-panel clearfix">
                <?php echo html_writer::tag('nav', $OUTPUT->navbar(), array('class' => 'breadcrumb-nav'.(($PAGE->course->id > 1) ? '' : ' hidden'))); ?>
                <?php echo html_writer::tag('div', $this->course_header(), array('id' => 'course-header')); ?>
            </div>
        </div>
    <?php endif; ?>
</header>
