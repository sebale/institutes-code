<?php if (isloggedin()) : ?>
    <section role="sidebar" class="left-sidebar">
        <?php echo theme_institutes_get_sidebar_topmenu(); ?>
        <?php echo theme_institutes_get_sidebar_bottommenu(); ?>
        <div class="side-pre-box clearfix">
            <div class="sidebar-title"><?php echo get_string('settings', 'theme_institutes'); ?><i class="ion-ios-close-outline" onclick="toggleSidePre();" title="<?php echo get_string('close', 'theme_institutes'); ?>"></i></div>
            <?php echo $OUTPUT->blocks('side-pre', $sidepre); ?>
        </div>
    </section>
<?php endif; ?>