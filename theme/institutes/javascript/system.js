var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

jQuery(window).ready(function() {

    $('.mobile-nav').click(function () {
        $('.left-sidebar').toggleClass('active');
        $('#page-content.row-fluid').toggleClass('active-sidebar');
    });

    $('.header-notifications-menu ul li:nth-last-child(3)').click(function (e) {
        e.preventDefault(false);
        $(this).toggleClass('active');
    });

    $('#page-my-index, #page-course-view-institutes').on('click', '.pickup', function () {
        var course_id = getUrlParameter('id');
        jQuery.ajax({
            async: false,
            url: "/theme/institutes/actions/ajax_report_get_last_action.php",
            type: "post",
            data: 'course_id=' + course_id,
            dataType: "json"
        }).done(function (data) {
            if(data.code == '200') {
                window.location = data.url;
            }
        });
    });
});

function toggleLeftSidebar(){
    if (!jQuery('.left-sidebar').hasClass('open') && !jQuery('.toggler-bg').length){

        if(!$('.left-sidebar.active').length) {
            jQuery('body').prepend('<div class="toggler-bg" onclick="closeMenu();"></div>');
            jQuery('.header-navbar').prepend('<div class="header-toggler-bg" onclick="closeMenu();"></div>');
        }

    } else if (jQuery('.left-sidebar').hasClass('open')) {
        jQuery('.toggler-bg').remove();

        jQuery('.header-toggler-bg').remove();
    }
    jQuery('.left-sidebar').toggleClass('open');

    if($('.left-sidebar.active').length) {
        $('#page-content.row-fluid.active-sidebar').toggleClass('open');
    }
}

function toggleSidePre(){
    if (!jQuery('.side-pre-box').hasClass('open') && !jQuery('.toggler-bg').length){
        jQuery('body').prepend('<div class="toggler-bg" onclick="closeMenu();"></div>');
        jQuery('.header-navbar').prepend('<div class="header-toggler-bg" onclick="closeMenu();"></div>');
    } else if (jQuery('.side-pre-box').hasClass('open')) {
         jQuery('.toggler-bg').remove();
        jQuery('.header-toggler-bg').remove();
    }
    jQuery('.side-pre-box').toggleClass('open');
    jQuery('.main-navigation li.preferences').toggleClass('active');
}

function toggleRightSidebar() {
    if (jQuery('.side-post').hasClass('open')){
        jQuery('#region-main-box').addClass('span12 pull-right').removeClass('span9');
        jQuery('#page').addClass('full-width');
        jQuery('.side-post').removeClass('open');
    } else {    
        jQuery('#region-main-box').addClass('span9').removeClass('span12 pull-right');
        jQuery('#page').removeClass('full-width');

        jQuery('.side-post').toggleClass('open');
        jQuery('.main-navigation li.activecourse').toggleClass('active');
    }
}

function closeMenu(){
    jQuery('.main-navigation li.preferences').removeClass('active');
    jQuery('.side-pre-box').removeClass('open');   
    jQuery('.left-sidebar').removeClass('open');
    jQuery('.toggler-bg').remove();
    jQuery('.header-toggler-bg').remove();
}

function module_completion(form, change_btn) {
    var status = form.find('input[name="state"]').val();
    var newstatus = form.find('input[name="completionstate"]').val();
    var title = form.find('input[name="title"]').val();
    var newtitle = form.find('input[name="newtitle"]').val();

    form.find('button').html('<i class="fa fa-spinner"></i>');
    $.ajax( {
      type: "POST",
      url: form.attr( 'action' ),
      data: form.serialize(),
      success: function() {
        form.find('input[name="state"]').val(newstatus);
        form.find('input[name="completionstate"]').val(status);
        form.find('input[name="title"]').val(newtitle);
        form.find('input[name="newtitle"]').val(title);
        form.find('button').html(newtitle);

        var status_btn = jQuery('.status-btn');
        status_btn.removeClass('btn-success');
        status_btn.removeClass('btn-danger');
        status_btn.removeClass('btn-warning');
        if (newstatus == '1'){
            status_btn.addClass('btn-success');
            status_btn.text('Complete');
            form.find('button').removeClass('btn-warning');
            form.find('button').addClass('btn-success');

            if(change_btn) {
                form.parent().parent().find('.btn-completion').removeClass('btn-info');
                form.parent().parent().find('.btn-completion').addClass('btn-success');
                form.parent().parent().find('.btn-completion').html('Complete');
            }
        } else {
            status_btn.addClass('btn-warning');
            status_btn.text('In process');
            form.find('button').removeClass('btn-success');
            form.find('button').addClass('btn-warning');

            if(change_btn) {
                form.parent().parent().find('.btn-completion').removeClass('btn-success');
                form.parent().parent().find('.btn-completion').addClass('btn-info');
                form.parent().parent().find('.btn-completion').html('Not Started');
            }
        }
      }
    } );
}

function toggleModuleCompletion() {
    var form = jQuery('#toggle_module_completion');
    module_completion(form, false);
}


function _toggleModuleCompletion(form) {
    module_completion(form, true);
}

