/*var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};*/

jQuery(window).ready(function(){

    // page quiz/attempt
    if(window.location.pathname == "/mod/quiz/attempt.php" || window.location.pathname == "/mod/quiz/review.php") {
        //var li = '';
        var questions = [];
        $('.que .info').first().append('<h3>FLAGGED QUESTIONS FOR REVIEW</h3>')
        $('.que .info .no').each(function() {
            //li += '<li>' + $(this).html() + '</li>';
            questions.push($(this).html());
            $(this).remove();
        });
        //$('.que .info').first().append("<ul>" + li + "</ul>");

        $('.content .formulation').each(function() {
            $(this).prepend('<h3>' + questions.shift() + '</h3>')
        });

        $('#region-main-box').on('click', '.questionflag', function () {
            $(this).toggleClass('active');
        });

        setTimeout( function() { $('#region-main-box .questionflagvalue').each(function () {
           if($(this).val() == 1) {
               $(this).parent().toggleClass('active');
           }
        })}, 2000);

        setInterval (function() {
            var attempt_id = getUrlParameter('attempt');
            if(attempt_id) {
                jQuery.ajax({
                    url: "/mod/quiz/ajax_action_questions_flag.php",
                    type: "POST",
                    data: {
                        attempt_id: attempt_id
                    },
                    dataType: "html"
                }).done(function (data) {
                    $('.que .info').first().find('ul').remove();
                    $('.que .info').first().append(data);
                });
            }
        }, 1000);
    }

    if(window.location.pathname == "/mod/quiz/summary.php") {
        $('.generaltable').find('img').each(function() {
            $(this).parents('.answersaved').find('.lastcol').html('<span class="flagged">Flagged</span>');
            $(this).remove();
        });

        //$('.generaltable').after('<div class="info"><h3>FLAGGED QUESTIONS FOR REVIEW</h3></div>')
        $('.submitbtns.mdl-align').first().toggleClass('global-pos');

        /*$('.activity-heading input').first().val('Finish')*/
    }

    if(window.location.pathname == "/mod/quiz/view.php") {
        $('h3.activity-heading').html('Quiz Completion')

        $('.attempts .attempt-item').click(function () {
            $(this).parent().toggleClass('active');

            if($(this).find('.col-action').html() == '+') {
                $(this).find('.col-action').html('-');
            } else {
                $(this).find('.col-action').html('+');
            }
        })
    }

    $('.header-notifications-menu').on('click', '.fa.fa-close.text-danger.pull-right', function () {
        var id = $(this).parents('.item').attr('data-id');
        var _this = this;
        jQuery.ajax({
            url: "/local/notifications/ajax_action_read_alert.php",
            type: "post",
            data: 'alert_id=' + id,
            dataType: "json"
        }).done(function (data) {
            if(data.code == '200') {
                $(_this).parent().parent().remove();
                var $num = $('.alert.notification').html();
                $('.alert.notification').html((($num-1)>0)?($num-1):0);
            }
        });

    })
});