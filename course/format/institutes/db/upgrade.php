<?php
// institutes.net
//
// institutes.net is built to work with any LMS designed in Moodle
// with the goal to deliver educational data analytics to single dashboard instantly.
// With power to turn this analytical data into simple and easy to read reports,
// institutes.net will become your primary reporting tool.
//
// Moodle
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//

function xmldb_format_institutes_upgrade($oldversion) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();
    $table = new xmldb_table('course_sections');
	$field = new xmldb_field('parent', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    
    $field = new xmldb_field('level', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    
    $field = new xmldb_field('parentssequence', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    return true;
}
