<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderer for outputting the institutes course format.
 *
 * @package format_institutes
 * @copyright 2012 Dan Poltawski
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @since Moodle 2.3
 */


defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot.'/course/format/renderer.php');

class format_institutes_course_content_header implements renderable {}
class format_institutes_course_content_footer implements renderable {}


/**
 * Basic renderer for institutes format.
 *
 * @copyright 2012 Dan Poltawski
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class format_institutes_renderer extends format_section_renderer_base {
    
    public $_sections = array();
    public $_sections_initialized = false;
    /**
     * Constructor method, calls the parent constructor
     *
     * @param moodle_page $page
     * @param string $target one of rendering target constants
     */
    public function __construct(moodle_page $page, $target) {
        parent::__construct($page, $target);

        // Since format_institutes_renderer::section_edit_controls() only displays the 'Set current section' control when editing mode is on
        // we need to be sure that the link 'Turn editing mode on' is available for a user who does not have any other managing capability.
        $page->set_other_editing_capability('moodle/course:setcurrentsection');
    }

    /**
     * Generate the starting container html for a list of sections
     * @return string HTML to output.
     */
    protected function start_section_list() {
        return html_writer::start_tag('ul', array('class' => 'institutes'));
    }

    /**
     * Generate the closing container html for a list of sections
     * @return string HTML to output.
     */
    protected function end_section_list() {
        return html_writer::end_tag('ul');
    }

    /**
     * Generate the title for this section page
     * @return string the page title
     */
    protected function page_title() {
        return get_string('topicoutline');
    }

    /**
     * Generate the section title, wraps it in a link to the section page if page is to be displayed on a separate page
     *
     * @param stdClass $section The course_section entry from DB
     * @param stdClass $course The course entry from DB
     * @return string HTML to output.
     */
    public function section_title($section, $course) {
        //return $this->render(course_get_format($course)->inplace_editable_render_section_name($section));
        $title = get_section_name($course, $section);
        $url = new moodle_url('/course/view.php',
                                array('id' => $course->id,
                                      'section' => $section->section));
        if ($section->level != 3){
            $title = html_writer::link($url, $title);   
        }
        return $title;
    }

    /**
     * Generate the section title to be displayed on the section page, without a link
     *
     * @param stdClass $section The course_section entry from DB
     * @param stdClass $course The course entry from DB
     * @return string HTML to output.
     */
    public function section_title_without_link($section, $course) {
        //return $this->render(course_get_format($course)->inplace_editable_render_section_name($section, false));
        return get_section_name($course, $section);
    }
    
    
    public function print_mainlevel_section_page($course, $sections, $mods, $modnames, $modnamesused) {
        global $PAGE;

        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();
        $csections = $this->get_course_sections($course);
        
        $context = context_course::instance($course->id);
        // Title with completion help icon.
        $completioninfo = new completion_info($course);
        echo $this->output->heading($this->page_title(), 2, 'accesshide');

        // Copy activity clipboard..
        echo $this->course_activity_clipboard($course, 0);

        echo html_writer::start_tag('div', array('class' => 'sections-containter'));

        echo $this->course_print_coursefile($course);

        echo $this->schedule_menu($course);

        // Now the list of sections..
        echo $this->start_section_list();
        
        $j = 1;
        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            $thissection->parent = (isset($this->_sections[$thissection->id]->parent)) ? $this->_sections[$thissection->id]->parent : 0;
            $thissection->level = (isset($this->_sections[$thissection->id]->level)) ? $this->_sections[$thissection->id]->level : 0;
            $thissection->parentssequence = (isset($this->_sections[$thissection->id]->parentssequence)) ? $this->_sections[$thissection->id]->parentssequence : '';
            
            if(intval($thissection->parent) > 0 or $section == 0){
                continue;
            }
            if ($section > $course->numsections) {
                // activities inside this section are 'orphaned', this section will be printed as 'stealth' below
                continue;
            }
            // Show the section if the user is permitted to access it, OR if it's not available
            // but there is some available info text which explains the reason & should display.
            $showsection = $thissection->uservisible ||
                    ($thissection->visible && !$thissection->available &&
                    !empty($thissection->availableinfo));
            if (!$showsection) {
                // If the hiddensections option is set to 'show hidden sections in collapsed
                // form', then display the hidden section message - UNLESS the section is
                // hidden by the availability system, which is set to hide the reason.
                if (!$course->hiddensections && $thissection->available) {
                    echo $this->section_hidden($section, $course->id);
                }

                continue;
            }

            echo $this->section_header($thissection, $course, false, 0, $j);
            echo $this->section_footer();
            $j++;
        }

        echo $this->end_section_list();
        
        echo html_writer::end_tag('div');

        echo $this->schedule_menu($course);

    }
    
    public function print_innerlevel_section_page($course, $sections, $mods, $modnames, $modnamesused, $displaysection) {
        global $PAGE;
        
        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();
        $csections = $this->get_course_sections($course);
        
        $parentsection = 0; $childsections = array();
        
        $currentsection = $modinfo->get_section_info($displaysection);
        $currentsection->parent = (isset($this->_sections[$currentsection->id]->parent)) ? $this->_sections[$currentsection->id]->parent : 0;
        if ($currentsection->parent > 0 and isset($this->_sections[$currentsection->parent]->section)){
            $parentsection = $modinfo->get_section_info($this->_sections[$currentsection->parent]->section);
        } else {
            $parentsection = $currentsection;
        }
        
        $context = context_course::instance($course->id);
        // Title with completion help icon.
        $completioninfo = new completion_info($course);
        echo $this->output->heading($this->page_title(), 2, 'accesshide');

        // Copy activity clipboard..
        echo $this->course_activity_clipboard($course, 0);

        // Now the list of sections..
        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            if($section == 0){
                continue;
            }
            
            // Show the section if the user is permitted to access it, OR if it's not available
            // but there is some available info text which explains the reason & should display.
            $showsection = $thissection->uservisible ||
                    ($thissection->visible && !$thissection->available &&
                    !empty($thissection->availableinfo));
            if (!$showsection) {
                // If the hiddensections option is set to 'show hidden sections in collapsed
                // form', then display the hidden section message - UNLESS the section is
                // hidden by the availability system, which is set to hide the reason.
                if (!$course->hiddensections && $thissection->available) {
                    echo $this->section_hidden($section, $course->id);
                }

                continue;
            }
            
            $thissection->parent = (isset($this->_sections[$thissection->id]->parent)) ? $this->_sections[$thissection->id]->parent : 0;
            if ($thissection->parent == $parentsection->id){
                $childsections[$section] = $thissection;
            }
        }
        
        echo $this->start_section_list();
        
        if ($parentsection){
            echo $this->section_header($parentsection, $course, false, $parentsection->section, 0);
            echo $this->section_footer();    
            
            if (count($childsections)){
                echo html_writer::start_tag('ul', array('class'=>'inner-section-box clearfix'));
                    foreach ($childsections as $section){
                        echo $this->section_header($section, $course, false, $currentsection->section, 0);
                        $innersections = $this->get_section_childs($course, $modinfo, $section->section);
                        if (count($innersections)){
                            echo $this->print_inner_sections($course, $innersections);
                        }
                        echo $this->section_footer();
                    }
                echo html_writer::end_tag('ul');
            }
        }
        
        echo $this->end_section_list();
        
    }
    
    public function get_section_childs($course, $modinfo, $displaysection) {
        
        $parentsection = $modinfo->get_section_info($displaysection);
        $childsections = array();
        
        if (!$this->_sections_initialized){
            $csections = $this->get_course_sections($course);
        }
        
        // Now the list of sections..
        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            if($section == 0){
                continue;
            }
            
            // Show the section if the user is permitted to access it, OR if it's not available
            // but there is some available info text which explains the reason & should display.
            $showsection = $thissection->uservisible ||
                    ($thissection->visible && !$thissection->available &&
                    !empty($thissection->availableinfo));
            if (!$showsection) {
                // If the hiddensections option is set to 'show hidden sections in collapsed
                // form', then display the hidden section message - UNLESS the section is
                // hidden by the availability system, which is set to hide the reason.
                if (!$course->hiddensections && $thissection->available) {
                    echo $this->section_hidden($section, $course->id);
                }

                continue;
            }
            
            
            $thissection->parent = (isset($this->_sections[$thissection->id]->parent)) ? $this->_sections[$thissection->id]->parent : 0;
            if ($thissection->parent == $parentsection->id){
                $childsections[$section] = $thissection;
            }
        }
        
        return $childsections;
    }
    
    public function get_all_section_childs($course, $modinfo, $displaysection, $sections = array()) {
        
        $parentsection = $modinfo->get_section_info($displaysection);
        $childsections = array();
        
        if (!$this->_sections_initialized){
            $csections = $this->get_course_sections($course);
        }
        
        // Now the list of sections..
        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            if($section == 0){
                continue;
            }
            
            // Show the section if the user is permitted to access it, OR if it's not available
            // but there is some available info text which explains the reason & should display.
            $showsection = $thissection->uservisible ||
                    ($thissection->visible && !$thissection->available &&
                    !empty($thissection->availableinfo));
            if (!$showsection) {
                continue;
            }
            
            $thissection->parent = (isset($this->_sections[$thissection->id]->parent)) ? $this->_sections[$thissection->id]->parent : 0;
            if ($thissection->parent == $parentsection->id){
                $childsections[$section] = $thissection;
            }
        }
        if (count($childsections)){
            foreach ($childsections as $child){
                $sections[$child->section] = $child;
                $sections = $this->get_all_section_childs($course, $modinfo, $child->section, $sections);
            }
        }
        
        return $sections;
    }
    
    public function get_section_completion($course, $sectionid) {
        global $CFG, $DB, $PAGE;
        
        $progress = array('status'=>'notstarted', 'progress'=>0);

        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();
        $currentsection = $modinfo->get_section_info($sectionid);
        $completioninfo = new completion_info($course);
        
        if (!$this->_sections_initialized){
            $csections = $this->get_course_sections($course);
        }
        
        $sections = array($currentsection->section=>$currentsection) + $this->get_all_section_childs($course, $modinfo, $currentsection->section, array());
        
        $modules = 0; $completed = 0; $viewed = 0;
        
        if (count($sections)){
            foreach ($sections as $section){
                if ($section->section == 0) continue;
                $section->level = (isset($this->_sections[$section->id]->level)) ? $this->_sections[$section->id]->level : 0;
                if ($section->level != 3) continue;
                if (!isset($modinfo->sections[$section->section])) continue;
                
                foreach ($modinfo->sections[$section->section] as $modnumber) {
                    
                    $mod = $modinfo->cms[$modnumber];
                    if (!$mod->uservisible) continue;
                    
                    // modules
                    $completion = $completioninfo->is_enabled($mod);
                    if ($completion == COMPLETION_TRACKING_NONE) {
                        continue;
                    }
                    $modules++;
                    
                    $completiondata = $completioninfo->get_data($mod, true);                    
                    // viewed
                    if ($completiondata->viewed){
                        $viewed++;
                    }

                    // completed
                    if ($completiondata->completionstate == COMPLETION_COMPLETE OR $completiondata->completionstate == COMPLETION_COMPLETE_PASS){
                        $completed++;
                    }

                }
                
            }
        }
        
        if ($modules > 0){
            if ($viewed > 0 or $completed > 0) $progress['status'] = 'inprogress';
            if ($completed > 0){
                if ($modules == $completed){
                    $progress['status'] = 'completed';
                    $progress['progress'] = 100;
                } else {
                    $progress['progress'] = round(($completed/$modules) * 100);
                }
            }
        }
        
        
        return $progress;
    }
    
    public function print_inner_sections($course, $sections){
        global $CFG, $OUTPUT;
        $o = '';
        
        if (count($sections)){
            $o .= html_writer::start_tag('ul', array('class' => 'inner-sections'));
                foreach ($sections as $section){
                    $progress = $this->get_section_completion($course, $section->section);
                    $url = new moodle_url('/course/view.php',
                                array('id' => $course->id,
                                      'section' => $section->section));
                    $sectionurl = $this->section_title($section, $course);
                    $o .= html_writer::tag('li', $sectionurl, array('class' => ($progress['status'] == 'completed') ? 'completed' : 'not-completed'));
                }
            $o .= html_writer::end_tag('ul');
        }
        
        return $o;
    }

    public function print_single_section_page($course, $sections, $mods, $modnames, $modnamesused, $displaysection) {
        global $PAGE;

        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();
        $csections = $this->get_course_sections($course);

        // Can we view the section in question?
        if (!($sectioninfo = $modinfo->get_section_info($displaysection))) {
            // This section doesn't exist
            print_error('unknowncoursesection', 'error', null, $course->fullname);
            return;
        }

        if (!$sectioninfo->uservisible) {
            if (!$course->hiddensections) {
                echo $this->start_section_list();
                echo $this->section_hidden($displaysection, $course->id);
                echo $this->end_section_list();
            }
            // Can't view this section.
            return;
        }
        
        // Copy activity clipboard..
        echo $this->course_activity_clipboard($course, $displaysection);
        // Start single-section div
        echo html_writer::start_tag('div', array('class' => 'single-section'));

        // The requested section page.
        $thissection = $modinfo->get_section_info($displaysection);
        $thissection->level = (isset($this->_sections[$thissection->id]->level)) ? $this->_sections[$thissection->id]->level : 0;
        $thissection->parentssequence = (isset($this->_sections[$thissection->id]->parentssequence)) ? $this->_sections[$thissection->id]->parentssequence : '';

        // Title with section navigation links.
        $sectionnavlinks = $this->get_nav_links($course, $modinfo->get_section_info_all(), $displaysection);
        $sectiontitle = '';
        $sectiontitle .= html_writer::start_tag('div', array('class' => ''));
        // Title attributes
        $classes = 'sectionname';
        if (!$thissection->visible) {
            $classes .= ' dimmed_text';
        }
        $sectionname = html_writer::tag('span', $this->section_title_without_link($thissection, $course));
        /*$sectiontitle .= $this->output->heading($sectionname, 2, $classes);

        $sectiontitle .= html_writer::end_tag('div');
        echo $sectiontitle;*/

        // Now the list of sections..
        echo $this->start_section_list();

        echo $this->section_header($thissection, $course, true, $displaysection);
        // Show completion help icon.
        $completioninfo = new completion_info($course);
        echo $completioninfo->display_help_icon();

        echo $this->courserenderer->course_section_cm_list($course, $thissection, $displaysection);
        echo $this->section_footer();
        echo $this->end_section_list();

        // Display section bottom navigation.
        $sectionbottomnav = '';
        $sectionbottomnav .= html_writer::start_tag('div', array('class' => 'section-navigation mdl-bottom'));
        if (!empty($sectionnavlinks['previous'])){
            $sectionbottomnav .= html_writer::tag('span', $sectionnavlinks['previous'], array('class' => 'btn'));
        }
        if (!empty($sectionnavlinks['next'])){
            $sectionbottomnav .= html_writer::tag('span', $sectionnavlinks['next'], array('class' => 'btn'));
        }
        //$sectionbottomnav .= html_writer::tag('div', $this->section_nav_selection($course, $sections, $displaysection),  array('class' => 'mdl-align'));
        $sectionbottomnav .= html_writer::end_tag('div');
        echo $sectionbottomnav;

        // Close single-section div.
        echo html_writer::end_tag('div');   
    }
    
    /**
     * Output the html for a multiple section page
     *
     * @param stdClass $course The course entry from DB
     * @param array $sections (argument not used)
     * @param array $mods (argument not used)
     * @param array $modnames (argument not used)
     * @param array $modnamesused (argument not used)
     */
    public function print_editing_sections_page($course, $sections, $mods, $modnames, $modnamesused, $displaysection) {
        global $PAGE;

        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();
        $csections = $this->get_course_sections($course);
        
        $currentsection = null;
        if (!empty($displaysection)) {
            $currentsection = $modinfo->get_section_info($displaysection);
            $currentsection->parent = (isset($this->_sections[$currentsection->id]->parent)) ? $this->_sections[$currentsection->id]->parent : 0;
            $currentsection->level = (isset($this->_sections[$currentsection->id]->level)) ? $this->_sections[$currentsection->id]->level : 0;    
        }


        $context = context_course::instance($course->id);
        // Title with completion help icon.
        $completioninfo = new completion_info($course);
        echo $completioninfo->display_help_icon();
        echo $this->output->heading($this->page_title(), 2, 'accesshide');

        // Copy activity clipboard..
        echo $this->course_activity_clipboard($course, 0);
        
        /*if (isset($currentsection->parent) and $currentsection->parent > 0){
            $parentsection = $modinfo->get_section_info($this->_sections[$currentsection->parent]->section);
            if ($parentsection){
                echo $this->section_header($parentsection, $course, false, $parentsection->section, 0);
                echo $this->section_footer();    
            }
        }*/

        // Now the list of sections..
        echo $this->start_section_list();

        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            if ($section == 0) {
                continue;
            }
            $thissection->parent = (isset($this->_sections[$thissection->id]->parent)) ? $this->_sections[$thissection->id]->parent : 0;
            $thissection->level = (isset($this->_sections[$thissection->id]->level)) ? $this->_sections[$thissection->id]->level : 0;
            $thissection->parentssequence = (isset($this->_sections[$thissection->id]->parentssequence)) ? $this->_sections[$thissection->id]->parentssequence : '';
            
            if (empty($displaysection)) {
                if(intval($thissection->parent) > 0 or $thissection->level > 0) continue;
            } else {
                if(isset($currentsection->id) and $thissection->parent != $currentsection->id) continue;
            }
            
            if ($section > $course->numsections) {
                // activities inside this section are 'orphaned', this section will be printed as 'stealth' below
                continue;
            }
            // Show the section if the user is permitted to access it, OR if it's not available
            // but there is some available info text which explains the reason & should display.
            $showsection = $thissection->uservisible ||
                    ($thissection->visible && !$thissection->available &&
                    !empty($thissection->availableinfo));
            if (!$showsection) {
                // If the hiddensections option is set to 'show hidden sections in collapsed
                // form', then display the hidden section message - UNLESS the section is
                // hidden by the availability system, which is set to hide the reason.
                if (!$course->hiddensections && $thissection->available) {
                    echo $this->section_hidden($section, $course->id);
                }

                continue;
            }

            
            echo $this->section_header($thissection, $course, false, 0);
            if ($thissection->uservisible) {
                if (isset($currentsection->level) and $thissection->level == 3){
                    echo $this->courserenderer->course_section_cm_list($course, $thissection, 0);
                }
                if ($thissection->level == 3){
                    echo $this->courserenderer->course_section_add_cm_control($course, $section, 0);
                }
            }
            echo $this->section_footer();
        }

        if ($PAGE->user_is_editing() and has_capability('moodle/course:update', $context)) {
            // Print stealth sections if present.
            foreach ($modinfo->get_section_info_all() as $section => $thissection) {
                if ($section <= $course->numsections or empty($modinfo->sections[$section])) {
                    // this is not stealth section or it is empty
                    continue;
                }
                echo $this->stealth_section_header($section);
                echo $this->courserenderer->course_section_cm_list($course, $thissection, 0);
                echo $this->stealth_section_footer();
            }

            echo $this->end_section_list();

            
            echo html_writer::start_tag('div', array('id' => 'changenumsections', 'class' => 'mdl-right'));

            // Increase number of sections.
            $straddsection = 'Create new section';
            $url = new moodle_url('/course/format/institutes/addsection.php',
                array('courseid' => $course->id,
                      'parent' => ((isset($currentsection->id)) ? $currentsection->id : 0)));
            
            $icon = $this->output->pix_icon('t/switch_plus', $straddsection);
            echo html_writer::link($url, $straddsection, array('class' => 'increase-sections btn', 'style'=>'margin-top:15px;'));

            echo html_writer::end_tag('div');
            
        }

    }

 
    public function get_course_sections($course) {
        global $PAGE, $DB;
        
        $sections = array();
        $allsections = $DB->get_records('course_sections', array('course'=>$course->id));
        if (count($allsections)){
            foreach($allsections as $section){
                $sections[$section->id] = $section;
            }
        }
        $this->_sections = $sections;
        $this->_sections_initialized = true;
        
        return $this->_sections;
    }
    
    /**
     * Output the html for a multiple section page
     *
     * @param stdClass $course The course entry from DB
     * @param array $sections (argument not used)
     * @param array $mods (argument not used)
     * @param array $modnames (argument not used)
     * @param array $modnamesused (argument not used)
     */
    public function print_multiple_section_page($course, $sections, $mods, $modnames, $modnamesused) {
        global $PAGE;

        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();
        $csections = $this->get_course_sections($course);

        $context = context_course::instance($course->id);
        // Title with completion help icon.
        $completioninfo = new completion_info($course);
        echo $completioninfo->display_help_icon();
        echo $this->output->heading($this->page_title(), 2, 'accesshide');

        // Copy activity clipboard..
        echo $this->course_activity_clipboard($course, 0);
        
        //echo html_writer::start_tag('div', array('class' => 'sections-containter'));
        
        echo $this->course_print_coursefile($course);

        // Now the list of sections..
        echo $this->start_section_list();

        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            if ($section == 0) {
                // 0-section is displayed a little different then the others
                /*if ($thissection->summary or !empty($modinfo->sections[0]) or $PAGE->user_is_editing()) {
                    echo $this->section_header($thissection, $course, false, 0);
                    echo $this->courserenderer->course_section_cm_list($course, $thissection, 0);
                    echo $this->courserenderer->course_section_add_cm_control($course, 0, 0);
                    echo $this->section_footer();
                }*/
                continue;
            }
            if ($section > $course->numsections) {
                // activities inside this section are 'orphaned', this section will be printed as 'stealth' below
                continue;
            }
            // Show the section if the user is permitted to access it, OR if it's not available
            // but there is some available info text which explains the reason & should display.
            $showsection = $thissection->uservisible ||
                    ($thissection->visible && !$thissection->available &&
                    !empty($thissection->availableinfo));
            if (!$showsection) {
                // If the hiddensections option is set to 'show hidden sections in collapsed
                // form', then display the hidden section message - UNLESS the section is
                // hidden by the availability system, which is set to hide the reason.
                if (!$course->hiddensections && $thissection->available) {
                    echo $this->section_hidden($section, $course->id);
                }

                continue;
            }

            if (!$PAGE->user_is_editing() && $course->coursedisplay == COURSE_DISPLAY_MULTIPAGE) {
                // Display section summary only.
                echo $this->section_summary($thissection, $course, null);
            } else {
                echo $this->section_header($thissection, $course, false, 0);
                if ($thissection->uservisible) {
                    echo $this->courserenderer->course_section_cm_list($course, $thissection, 0);
                    
                    $thissection->parent = (isset($this->_sections[$thissection->id]->parent)) ? $this->_sections[$thissection->id]->parent : 0;
                    if(intval($thissection->parent) > 0 and intval($this->_sections[$thissection->id]->level) == 3){
                        echo $this->courserenderer->course_section_add_cm_control($course, $section, 0);   
                    }
                }
                echo $this->section_footer();
            }
        }

        if ($PAGE->user_is_editing() and has_capability('moodle/course:update', $context)) {
            // Print stealth sections if present.
            foreach ($modinfo->get_section_info_all() as $section => $thissection) {
                if ($section <= $course->numsections or empty($modinfo->sections[$section])) {
                    // this is not stealth section or it is empty
                    continue;
                }
                echo $this->stealth_section_header($section);
                echo $this->courserenderer->course_section_cm_list($course, $thissection, 0);
                echo $this->stealth_section_footer();
            }

            echo $this->end_section_list();

            echo html_writer::start_tag('div', array('id' => 'changenumsections', 'class' => 'mdl-right'));

            // Increase number of sections.
            $straddsection = get_string('increasesections', 'moodle');
            $url = new moodle_url('/course/changenumsections.php',
                array('courseid' => $course->id,
                      'increase' => true,
                      'sesskey' => sesskey()));
            $icon = $this->output->pix_icon('t/switch_plus', $straddsection);
            echo html_writer::link($url, $icon.get_accesshide($straddsection), array('class' => 'increase-sections'));

            if ($course->numsections > 0) {
                // Reduce number of sections sections.
                $strremovesection = get_string('reducesections', 'moodle');
                $url = new moodle_url('/course/changenumsections.php',
                    array('courseid' => $course->id,
                          'increase' => false,
                          'sesskey' => sesskey()));
                $icon = $this->output->pix_icon('t/switch_minus', $strremovesection);
                echo html_writer::link($url, $icon.get_accesshide($strremovesection), array('class' => 'reduce-sections'));
            }

            echo html_writer::end_tag('div');
        } else {
            echo $this->end_section_list();
        }
        
        //echo html_writer::end_tag('div');
        //echo $this->schedule_menu($course);

    }
    
    protected function schedule_menu($course) {
        
        echo html_writer::start_tag('div', array('class' => 'sections-menu'));
        
        $context = context_course::instance($course->id);
        $fs = get_file_storage();
        $files = $fs->get_area_files($context->id, 'format_institutes', 'schedule', $course->id);
        if (count($files)){
            foreach ($files as $file) {
                $filename = $file->get_filename();
                $filetype = $file->get_mimetype();
                if ($filename == '.' or !$filetype) continue;

                $url = moodle_url::make_pluginfile_url($context->id, 'format_institutes', 'schedule', $course->id, '/', $filename);
                echo html_writer::link($url->out(), get_string('schedule', 'format_institutes'), array('class' => 'sect', 'target'=>'_blank'));
            }
        }
        
        $url = new moodle_url('/local/gradebook/index.php', array('id' => $course->id));
        echo html_writer::link($url, get_string('schedule', 'format_institutes'), array('class' => 'sect schedule_item'));
        echo html_writer::link($url, get_string('gradebook', 'format_institutes'), array('class' => 'sect'));
        echo html_writer::link($url, get_string('pickup', 'format_institutes'), [
            'class' => 'sect active pickup',
            'onClick' => 'return false;'
        ]);
        echo html_writer::end_tag('div');
    }
    
    protected function section_header($section, $course, $onsectionpage, $sectionreturn=null, $sectionid = 0) {
        global $PAGE, $CFG, $DB;

        $o = '';
        $currenttext = '';
        $sectionstyle = '';
        
        if ($section->section != 0) {
            // Only in the non-general sections.
            if (!$section->visible) {
                $sectionstyle = ' hidden';
            } else if (course_get_format($course)->is_section_current($section)) {
                $sectionstyle = ' current';
            }
        }
        
        $slevel = (isset($this->_sections[$section->id]->level)) ? ' section-level-'.intval($this->_sections[$section->id]->level) : '';
        $section->level = (isset($this->_sections[$section->id]->level)) ? intval($this->_sections[$section->id]->level) : 0;
        $section->parentssequence = (isset($this->_sections[$section->id]->parentssequence)) ? $this->_sections[$section->id]->parentssequence : '';
        
        $params = array(
            'id' => 'section-'.$section->section,
            'class' => 'section main clearfix'.$sectionstyle.$slevel, 'role'=>'region',
            'aria-label'=> get_section_name($course, $section)
        );
        
        if ($section->section == $sectionreturn){
            $params['class'] .= ' current';
        }
        if (!$PAGE->user_is_editing()){
            $section->progress = $this->get_section_completion($course, $section->section);
            $params['class'] .= ' '.$section->progress['status'];
        
            if (($section->level == 0 and $section->section != $sectionreturn)){
                $params['onclick'] = "location='".$CFG->wwwroot.'/course/view.php?id='.$course->id.'&section='.$section->section."'";
            } elseif (($section->level == 0 and $section->section == $sectionreturn)){
                $params['class'] .= ' main-section';
            } elseif ($section->level == 1){
                $params['onclick'] = "location='".$CFG->wwwroot.'/course/view.php?id='.$course->id.'&section='.$section->section."'";
            }
        }
        
        $o.= html_writer::start_tag('li', $params);

        // Create a span that contains the section title to be used to create the keyboard section move menu.
        $o .= html_writer::tag('span', get_section_name($course, $section), array('class' => 'hidden sectionname'));
        
        if (!$PAGE->user_is_editing()){
            $leftcontent = $this->section_left_content($section, $course, $onsectionpage);
            $o.= html_writer::tag('div', $leftcontent, array('class' => 'left side'));
        } else {
            $url = new moodle_url('/course/format/institutes/movesection.php',
                                array('courseid' => $course->id,
                                      'section' => $section->id,
                                      'action' => 'up'));
            $moveupicon = '<span class="fa fa-angle-up"></span>';
            $leftcontent = html_writer::link($url, $moveupicon, array('class' => 'section-move move-up', 'title'=>'Move section up'));
            
            $url = new moodle_url('/course/format/institutes/movesection.php',
                                array('courseid' => $course->id,
                                      'section' => $section->id,
                                      'action' => 'down'));
            $movedownicon = '<span class="fa fa-angle-down"></span>';
            $leftcontent .= html_writer::link($url, $movedownicon, array('class' => 'section-move move-down', 'title'=>'Move section down'));
            $o.= html_writer::tag('div', $leftcontent, array('class' => 'left-side'));
        }

        $rightcontent = $this->section_right_content($section, $course, $onsectionpage);
        $o.= html_writer::tag('div', $rightcontent, array('class' => 'right side'));
        $o.= html_writer::start_tag('div', array('class' => 'content'));

        // When not on a section page, we display the section titles except the general section if null
        $hasnamenotsecpg = (!$onsectionpage && ($section->section != 0 || !is_null($section->name)));

        // When on a section page, we only display the general section title, if title is not the default one
        $hasnamesecpg = ($onsectionpage && ($section->section == 0 && !is_null($section->name)));

        $classes = ' accesshide';
        if ($hasnamenotsecpg || $hasnamesecpg) {
            $classes = '';
        }

        if (!$PAGE->user_is_editing() and $section->level == 0) {
            if ($section->level == 0 and $sectionid == 0) {
                $sectionid = $this->get_sectionid($section);
            }
            $sectionname = html_writer::tag('span', ((strlen($sectionid) > 1) ? $sectionid : '0'.$sectionid));
            $title = html_writer::tag('span', get_section_name($course, $section), ['class' => 'title']);
            $o .= $this->output->heading($sectionname . ' ' . $title, 4, 'sectionid');
        } else {
            $sectionname = html_writer::tag('span', $this->section_title($section, $course));
            $o .= $this->output->heading($sectionname, 3, 'sectionname' . $classes);
        }
        
        $o .= html_writer::start_tag('div', array('class' => 'summary'));
        $o .= $this->format_summary_text($section);
        $o .= html_writer::end_tag('div');

        $context = context_course::instance($course->id);
        $o .= $this->section_availability_message($section,
                has_capability('moodle/course:viewhiddensections', $context));
        
        if (($section->level == 0 and !$sectionreturn) and !$PAGE->user_is_editing()){
            $o .= $this->section_completion_status($course, $section);
        }

        return $o;
    }
    
    protected function get_sectionid($section) {
        $sectionid = 0;
        
        $level_sections = array();
        if (count($this->_sections)){
            foreach ($this->_sections as $ssection){
                $level_sections[$ssection->level][$ssection->section] = $ssection;
            }
        }
        
        if (count($level_sections[$section->level])){
            $i = 0;
            foreach ($level_sections[$section->level] as $ls){
                if ($ls->id == $section->id) {
                    $sectionid = $i;
                    break;
                }
                $i++;
            }
        }
        
        return $sectionid;
    }
    
    protected function section_completion_status($course, $section) {
        $o = '';
        
        $o .= html_writer::start_tag('div', array('class'=>'section-completion-status'));
            $o .= html_writer::tag('label', get_string('completed', 'format_institutes').':');
            $o .= $section->progress['progress'].'%';
        $o .= html_writer::end_tag('div');
        
        return $o;
    }

    /**
     * Generate the edit control items of a section
     *
     * @param stdClass $course The course entry from DB
     * @param stdClass $section The course_section entry from DB
     * @param bool $onsectionpage true if being printed on a section page
     * @return array of edit control items
     */
    protected function section_edit_control_items($course, $section, $onsectionpage = false) {
        global $PAGE;

        if (!$PAGE->user_is_editing()) {
            return array();
        }

        $coursecontext = context_course::instance($course->id);

        if ($onsectionpage) {
            $url = course_get_url($course, $section->section);
        } else {
            $url = course_get_url($course);
        }
        $url->param('sesskey', sesskey());

        $isstealth = $section->section > $course->numsections;
        $controls = array();


        if (!isset($this->_sections[$section->id]->level) or (isset($this->_sections[$section->id]->level) and $this->_sections[$section->id]->level != 3)){
            $editeurl = new moodle_url('/course/format/institutes/addsection.php', array("courseid"=>$course->id, "parent"=>$section->id));

            $controls['addsection'] = array('url' => $editeurl, "icon" => 'i/manual_item',
                                                   'name' => 'Add section',
                                                   'pixattr' => array('class' => '', 'alt' => 'Add section'),
                                                   'attr' => array('class' => 'editing_addsection', 'title' => 'Add section'));
        }


        if (!$isstealth && $section->section && has_capability('moodle/course:setcurrentsection', $coursecontext)) {
            if ($course->marker == $section->section) {  // Show the "light globe" on/off.
                $url->param('marker', 0);
                $markedthistopic = get_string('markedthistopic');
                $highlightoff = get_string('highlightoff');
                $controls['highlight'] = array('url' => $url, "icon" => 'i/marked',
                                               'name' => $highlightoff,
                                               'pixattr' => array('class' => '', 'alt' => $markedthistopic),
                                               'attr' => array('class' => 'editing_highlight', 'title' => $markedthistopic));




            } else {
                $url->param('marker', $section->section);
                $markthistopic = get_string('markthistopic');
                $highlight = get_string('highlight');
                $controls['highlight'] = array('url' => $url, "icon" => 'i/marker',
                                               'name' => $highlight,
                                               'pixattr' => array('class' => '', 'alt' => $markthistopic),
                                               'attr' => array('class' => 'editing_highlight', 'title' => $markthistopic));
            }
        }

        $parentcontrols = parent::section_edit_control_items($course, $section, $onsectionpage);

        // If the edit key exists, we are going to insert our controls after it.
        if (array_key_exists("edit", $parentcontrols)) {
            $merged = array();
            // We can't use splice because we are using associative arrays.
            // Step through the array and merge the arrays.
            foreach ($parentcontrols as $key => $action) {
                $merged[$key] = $action;
                if ($key == "edit") {
                    // If we have come to the edit key, merge these controls here.
                    $merged = array_merge($merged, $controls);
                }
            }

            return $merged;
        } else {
            return array_merge($controls, $parentcontrols);
        }
    }


    public function course_print_coursefile($course)
    {
        global $PAGE, $CFG;

        require_once($CFG->dirroot . '/filter/mediaplugin/filter.php');
        $filterplugin = new filter_mediaplugin(null, array());

        $context = context_course::instance($course->id);
        $contentimages = '';
        if ($course->infotext) {
            $contentimages .= html_writer::tag('div', $course->infotext, array('class' => 'course-file-box embed'));
        } else {
            $fs = get_file_storage();
            $image_files = array();
            $imgfiles = $fs->get_area_files($context->id, 'format_institutes', 'thumbnail', $course->id);
            $j = 1;
            foreach ($imgfiles as $file) {
                $filename = $file->get_filename();
                $filetype = $file->get_mimetype();
                $itemid = $course->id . "_" . $j++;
                if ($filename == '.' or !$filetype) continue;

                if (stripos($filetype, 'video') !== FALSE) {
                    $url = moodle_url::make_pluginfile_url($context->id, 'format_institutes', 'thumbnail', $course->id, '/', $filename);
                    $contentimages .= $filterplugin->filter('<div class="course-info-playerbox" id="' . $itemid . '"><a href="' . $url . '">' . $filename . '</a></div>');
                } else if (stripos($filetype, 'image') !== FALSE) {
                    $url = moodle_url::make_pluginfile_url($context->id, 'format_institutes', 'thumbnail', $course->id, '/', $filename);
                    $contentimages .= html_writer::empty_tag('img', array('src' => $url->out()));
                }
            }

            if (!empty($contentimages)) {
                $contentimages = html_writer::tag('div', $contentimages, array('class' => 'course-file-box'));
            }
        }

        return $contentimages;

    }
    
    protected function render_format_institutes_course_content_header(format_institutes_course_content_header $a) {
        global $PAGE, $USER, $DB;
        
        $o = '';
        $currentsection = null;
        $course = $PAGE->course;
        
        if ($PAGE->user_is_editing() and !isset($PAGE->cm)) return $o;
        
        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();
        $csections = $this->get_course_sections($course);
        
        $displaysection = optional_param('section', 0, PARAM_INT);
        if ($displaysection > 0){
            $currentsection = $modinfo->get_section_info($displaysection);
            $currentsection->level = (isset($this->_sections[$currentsection->id]->level)) ? $this->_sections[$currentsection->id]->level : 0;
            if ($currentsection->level == 3){
                $parent = $DB->get_record('course_sections', array('id'=>$currentsection->parent));
                if (isset($parent->section)){
                    $currentsection = $modinfo->get_section_info($parent->section);
                    $currentsection->level = (isset($this->_sections[$currentsection->id]->level)) ? $this->_sections[$currentsection->id]->level : 0;
                }
            }
        } elseif (isset($PAGE->cm->section)){
            $cm_section = $DB->get_record('course_sections', array('id'=>$PAGE->cm->section));
            if (isset($cm_section->id)){
                if ($cm_section->parent > 0 and $cm_section->level == 3){
                    $parent = $DB->get_record('course_sections', array('id'=>$cm_section->parent));
                    if (isset($parent->section)){
                        $currentsection = $modinfo->get_section_info($parent->section);
                        $currentsection->level = (isset($this->_sections[$currentsection->id]->level)) ? $this->_sections[$currentsection->id]->level : 0;
                    }
                }
            }   
        }
        if (!$currentsection) return $o;
        if ($currentsection->level < 2) return $o;
        
        $o .= html_writer::tag('h2', get_section_name($course, $currentsection), array('class' => 'section-title'));
        $o .= html_writer::tag('div', $this->format_summary_text($currentsection), array('class' => 'section-summary'));
        
        $sections = $this->get_section_childs($course, $modinfo, $currentsection->section);
        
        if (count($sections)){
            $j = 1;

            $o .= html_writer::start_tag('div', ['class' => 'section-menu-div']);
            $o .= html_writer::tag('p', 'Sections');
            $o .= html_writer::tag('hr', '');
            $o .= html_writer::start_tag('ul', array('class' => 'section-menu'));
            foreach ($sections as $section){
                $section->progress = $this->get_section_completion($course, $section->section);
                $o .= html_writer::start_tag('li', array('class' => 'menu-item '.$section->progress['status'].(($section->section == $displaysection or (isset($cm_section->section) and $cm_section->section == $section->section) or ($j == 1 and $section->parent == $currentsection->id and $currentsection->section == $displaysection)) ? ' active' : '')));
                    $url = new moodle_url('/course/view.php',
                                array('id' => $course->id,
                                      'section' => $section->section));
                    $toggler = html_writer::tag('span', '', array('class' => 'section-menu-toggler fa fa-play', 'onclick'=>'jQuery(this).parent().toggleClass("open");'));
                    $o .= html_writer::link($url, $this->section_title($section, $course)).$toggler;
                    $section = $modinfo->get_section_info($section->section);
                    
                    if (!empty($modinfo->sections[$section->section])) {
                        $o .= html_writer::start_tag('ul', array('class' => 'section-modules'));
                        foreach ($modinfo->sections[$section->section] as $modnumber) {
                            $mod = $modinfo->cms[$modnumber];

                            if ((!$mod->uservisible && empty($mod->availableinfo)) || !$mod->url) {
                                continue;
                            }
                            $instancename = $mod->get_formatted_name();
                            $onclick = htmlspecialchars_decode($mod->onclick, ENT_QUOTES);
                            
                            $o .= html_writer::start_tag('li', array('class' => 'activity-item'.((isset($PAGE->cm->id) and $PAGE->cm->id == $mod->id) ? ' active' : '')));
                                $o .= html_writer::link($mod->url, $instancename, array('onclick' => $onclick));
                            $o .= html_writer::end_tag('li');
                        }
                        $o .= html_writer::end_tag('ul');
                    }
                $o .= html_writer::end_tag('li');
                $j++;
            }
            $o .= html_writer::end_tag('ul');
            $o .= html_writer::end_tag('div');
        }
        
        return $o;
    }
    
    protected function render_format_institutes_course_content_footer(format_institutes_course_content_footer $a) {
        global $PAGE, $USER, $DB, $CFG;
        
        $o = '';
        $status = 'notstarted'; $type = '';
        $course = $PAGE->course;
        
        if ($PAGE->user_is_editing()) return $o;
        
        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();
        $completioninfo = new completion_info($course);
        
        if (isset($PAGE->cm->id)){
            $mod = $modinfo->cms[$PAGE->cm->id];
            
            if (!$mod->uservisible && empty($mod->availableinfo)) {
                return $o;
            }
            
            $completion = $completioninfo->is_enabled($mod);
            if ($completion == COMPLETION_TRACKING_NONE) {
                return $o;
            }

            $completiondata = $completioninfo->get_data($mod, true);
            $completionicon = '';
            
            if ($completion == COMPLETION_TRACKING_MANUAL) {
                switch($completiondata->completionstate) {
                    case COMPLETION_INCOMPLETE:
                        $status = 'inprocess'; break;
                    case COMPLETION_COMPLETE:
                        $status = 'complete'; break;
                }
            } else { // Automatic
                switch($completiondata->completionstate) {
                    case COMPLETION_INCOMPLETE:
                        $status = 'inprocess'; break;
                    case COMPLETION_COMPLETE:
                        $status = 'complete'; break;
                    case COMPLETION_COMPLETE_PASS:
                        $status = 'complete'; break;
                    case COMPLETION_COMPLETE_FAIL:
                        $status = 'fail'; break;
                }
            }
            if ($completiondata->viewed and $status == ''){
                $status = 'inprocess';
            }
            
            $o .= html_writer::start_tag('div', array('class' => 'course-content-footer-inner'));
            
            if ($completion == COMPLETION_TRACKING_MANUAL) {
               
                $newstate =
                    $completiondata->completionstate == COMPLETION_COMPLETE
                    ? COMPLETION_INCOMPLETE
                    : COMPLETION_COMPLETE;
                
                $title = ($completiondata->completionstate == COMPLETION_COMPLETE) ? 'Mark incomplete' : 'Mark complete';
                $newtitle = ($completiondata->completionstate != COMPLETION_COMPLETE) ? 'Mark incomplete' : 'Mark complete';
                $btnclass = ($completiondata->completionstate == COMPLETION_COMPLETE) ? 'btn-warning' : 'btn-success';
                
                $o .= html_writer::start_tag('form', array('method' => 'post',
                    'action'=> new moodle_url('/course/togglecompletion.php'),
                    'class' => 'togglecompletion',
                    'id'    => 'toggle_module_completion'));
                $o .= html_writer::empty_tag('input', array(
                    'type' => 'hidden', 'name' => 'id', 'value' => $mod->id));
                $o .= html_writer::empty_tag('input', array(
                    'type' => 'hidden', 'name' => 'sesskey', 'value' => sesskey()));
                $o .= html_writer::empty_tag('input', array(
                    'type' => 'hidden', 'name' => 'modulename', 'value' => $mod->name));
                $o .= html_writer::empty_tag('input', array(
                    'type' => 'hidden', 'name' => 'completionstate', 'value' => $newstate));
                $o .= html_writer::empty_tag('input', array(
                    'type' => 'hidden', 'name' => 'state', 'value' => $completiondata->completionstate));
                $o .= html_writer::empty_tag('input', array(
                    'type' => 'hidden', 'name' => 'title', 'value' => $title));
                $o .= html_writer::empty_tag('input', array(
                    'type' => 'hidden', 'name' => 'newtitle', 'value' => $newtitle));
                $o .= html_writer::tag('button', $title, array('type'=>'button', 'onclick'=>'toggleModuleCompletion(); return false;', 'class'=>'btn btn-inverse '.$btnclass));
                $o .= html_writer::end_tag('form');
            }
            
            if (!empty($status)){
                $classes = 'status-btn btn';
                if ($status == 'complete'){
                    $classes .= ' btn-success';
                    $statustitle = 'Complete';
                } elseif ($status == 'fail'){
                    $classes .= ' btn-danger';
                    $statustitle = 'Fail';
                } else {
                    $classes .= ' btn-warning';
                    $statustitle = 'In process';
                }
                $o .= html_writer::tag('button', $statustitle, array('class'=>$classes, 'id'=>'status_btn'));
            }
            
            $o .= html_writer::end_tag('div');
            
            //$navlinks = $this->get_activity_nav_links($course, $mod);
            if (!$this->_sections_initialized){
                $csections = $this->get_course_sections($course);
            }
            $displaysection = (isset($this->_sections[$mod->section])) ? $this->_sections[$mod->section]->section : 1;
            $sectionnavlinks = $this->get_nav_links($course, $modinfo->get_section_info_all(), $displaysection);
            $o .= html_writer::start_tag('div', array('class' => 'course-content-footer-navbuttons'));
                /*if (!empty($navlinks['previous'])){
                    $o .= $navlinks['previous'];
                }
                if (!empty($navlinks['next'])){
                    $o .= $navlinks['next'];
                }*/
                if (!empty($sectionnavlinks['previous'])){
                    $o .= html_writer::tag('span', $sectionnavlinks['previous'], array('class' => 'btn prev-button'));
                }
                if (!empty($sectionnavlinks['next'])){
                    $o .= html_writer::tag('span', $sectionnavlinks['next'], array('class' => 'btn next-button'));
                }
            $o .= html_writer::end_tag('div');
            
        }
        
        return $o;
    }
    
    
    
    /**
     * Generate next/previous section links for naviation
     *
     * @param stdClass $course The course entry from DB
     * @param array $sections The course_sections entries from the DB
     * @param int $sectionno The section number in the coruse which is being dsiplayed
     * @return array associative array with previous and next section link
     */
    protected function get_nav_links($course, $sections, $sectionno) {
        // FIXME: This is really evil and should by using the navigation API.
        $course = course_get_format($course)->get_course();
        $canviewhidden = has_capability('moodle/course:viewhiddensections', context_course::instance($course->id))
            or !$course->hiddensections;

        $links = array('previous' => '', 'next' => '');
        $back = $sectionno - 1;
        while ($back > 0 and empty($links['previous'])) {
            $sections[$back]->level = (isset($this->_sections[$sections[$back]->id]->level)) ? $this->_sections[$sections[$back]->id]->level : 0;
            if (($canviewhidden || $sections[$back]->uservisible) and $sections[$back]->level == 3) {
                $params = array();
                if (!$sections[$back]->visible) {
                    $params = array('class' => 'dimmed_text');
                }
                $previouslink = html_writer::tag('span', $this->output->larrow(), array('class' => 'larrow'));
                $previouslink .= get_string('previous');
                $links['previous'] = html_writer::link(new moodle_url('/course/view.php', array('id' => $course->id,
                                      'section' => $back)), $previouslink, $params);
            }
            $back--;
        }
        
        $forward = $sectionno + 1;
        while ($forward <= $course->numsections and empty($links['next'])) {
            $sections[$forward]->level = (isset($this->_sections[$sections[$forward]->id]->level)) ? $this->_sections[$sections[$forward]->id]->level : 0;
            if (($canviewhidden || $sections[$forward]->uservisible) and $sections[$forward]->level == 3) {
                $params = array();
                if (!$sections[$forward]->visible) {
                    $params = array('class' => 'dimmed_text');
                }
                $nextlink = get_string('next');
                $nextlink .= html_writer::tag('span', $this->output->rarrow(), array('class' => 'rarrow'));
                $links['next'] = html_writer::link(new moodle_url('/course/view.php', array('id' => $course->id,
                                      'section' => $forward)), $nextlink, $params);
            }
            $forward++;
        }
        
        return $links;
    }

    protected function has_nav_mod($mod) {
        if ((!$mod->uservisible || !$mod->has_view()) && !has_capability('moodle/course:viewhiddenactivities', $mod->context)) {
            return false;
        }

        if($mod->modname == 'label') {
            return false;
        }

        if(!$mod->url) {
            return false;
        }

        return true;
    }
    
    protected function get_activity_nav_links($course, $cm) {
        $course = course_get_format($course)->get_course();
        $modinfo = get_fast_modinfo($course);

        $links = [
            'previous' => '',
            'next' => ''
        ];
        
        $result = [
            'firstmodules' => false,
            'prev_mod' => 0,
            'next_mod' => 0,
            'break_prev' => false,
            'break_next' => false
        ];
        
        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            if (!$thissection->visible) continue;

            if (isset($modinfo->sections[$thissection->section])) {
                foreach ($modinfo->sections[$thissection->section] as $modnumber) {
                    $mod = $modinfo->cms[$modnumber];

                    if(!$this->has_nav_mod($mod)) {
                        continue;
                    }

                    if ($cm->id == $mod->id) {
                        if (!$result['break_prev']) {
                            $result['firstmodules'] = true;
                        }
                        $result['break_prev'] = true;
                        $result['break_next'] = true;
                    } else {
                        if (!$result['break_prev']) {
                            $result['prev_mod'] = $mod;
                        }
                        if ($result['break_next']) {
                            $result['next_mod'] = $mod;
                            break;
                        }
                    }
                }
            }

            if (($result['prev_mod'] || $result['firstmodules']) && $result['next_mod']){
                break;
            }
        }
        
        if($result['prev_mod']) {
            $onclick = htmlspecialchars_decode($result['prev_mod']->onclick, ENT_QUOTES);
            $links['previous'] = html_writer::link($result['prev_mod']->url, get_string('previous'), [
                'class' => 'btn prev-button',
                'onclick' => $onclick
            ]);
        }

        if($result['next_mod']) {
            $onclick = htmlspecialchars_decode($result['next_mod']->onclick, ENT_QUOTES);
            $links['next'] = html_writer::link($result['next_mod']->url, get_string('next'), [
                'class' => 'btn next-button',
                'onclick' => $onclick
            ]);
        }
        return $links;
    }
    
    public function get_sections_sequense($course, $modinfo, $rootsection = 0) {
        
        if ($rootsection > 0){
            $parentsection = $modinfo->get_section_info($rootsection);
        }
        
        $sectionlist = array();
        
        if (!$this->_sections_initialized){
            $csections = $this->get_course_sections($course);
        }
        
        // Now the list of sections..
        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            if($section == 0){
                continue;
            }
            
            // Show the section if the user is permitted to access it, OR if it's not available
            // but there is some available info text which explains the reason & should display.
            $showsection = $thissection->uservisible ||
                    ($thissection->visible && !$thissection->available &&
                    !empty($thissection->availableinfo));
            if (!$showsection) {
                continue;
            }
            
            $thissection->parent = (isset($this->_sections[$thissection->id]->parent)) ? $this->_sections[$thissection->id]->parent : 0;
            
            $sectionlist[$thissection->parent]['childs'][$thissection->id] = $thissection;   
        }
        
        return $sectionlist;
    }
    
}



