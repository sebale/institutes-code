<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This script allows the number of sections in a course to be increased
 * or decreased, redirecting to the course page.
 *
 * @package core_course
 * @copyright 2012 Dan Poltawski
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @since Moodle 2.3
 */

require_once(dirname(__FILE__).'/../../../config.php');
require_once($CFG->dirroot.'/course/lib.php');

$courseid = required_param('courseid', PARAM_INT);
$parentid = required_param('parent', PARAM_INT);

$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$parent = $DB->get_record('course_sections', array('id' => $parentid), '*', MUST_EXIST);
$courseformatoptions = course_get_format($course)->get_format_options();
$index = $courseformatoptions['numsections'] + 1;
$PAGE->set_url('/course/format/institutes/addsection.php', array('courseid' => $courseid));


require_login($course);
require_capability('moodle/course:update', context_course::instance($course->id));

$format_renderer = $PAGE->get_renderer('format_institutes');

$section = new stdClass();
$section->course = $courseid;
$section->section = $index;
$section->parent = $parentid;
$section->visible = 1;
if ($section->parent > 0){
    $parent_level = $DB->get_record('course_sections', array('id'=>$parentid));
            
    $section->level = $parent_level->level+1;
    $section->parentssequence = $parentid;
    if ($parent_level->parent > 0){
        $section->parentssequence = $parent_level->parentssequence.','.$parentid;                    
    }
} else {
    $section->parentssequence = '';
    $section->level = 0;
}

update_course((object)array('id' => $course->id, 'numsections' => $index));
$section->id = $DB->insert_record('course_sections', $section);
rebuild_course_cache($courseid, true);

if (isset($parent_level->id)){
    $modinfo = get_fast_modinfo($course);
    $child_sections = $format_renderer->get_section_childs($course, $modinfo, $parent_level->section);

    if (count($child_sections) > 1){
        move_section_to($course, $section->section, ($parent_level->section + count($child_sections)));
        
    } else {
        //echo (($parent_level->section));
        move_section_to($course, $section->section, ($parent_level->section-2));
    }
    $response = course_get_format($course)->ajax_section_move();
}
rebuild_course_cache($courseid, true);

$url = new moodle_url('/course/editsection.php', array("id"=>$section->id, "sr"=>$parent->section, "parent"=>$parent->id));

redirect($url);
