<?php

require_once("../../config.php");
require_once("lib.php");

require_login();

$course_id = optional_param('id', 0, PARAM_INT);
$courses = get_grade_book($course_id);

$title = "Gradebook";

global $PAGE, $OUTPUT, $CFG;

$PAGE->set_context(context_system::instance());
$PAGE->set_url(new moodle_url("/local/gradebook/index.php", array()));
$PAGE->requires->jquery();
$PAGE->navbar->add($title, new moodle_url('/mod/ecampus/gradebook.php'));
$PAGE->set_title($title);
$PAGE->set_pagelayout('course');
$PAGE->set_heading($title);

$PAGE->requires->js('/local/gradebook/assets/js/script.js', true);
$PAGE->requires->css('/local/gradebook/assets/css/style.css', true);

echo $OUTPUT->header();

?>

<div class="gradebook">
	<?php echo $OUTPUT->heading($title); ?>

	<?= html_writer::start_tag('ul', ['class' => 'courses']) ?>
		<?php foreach($courses as $key => $course): ?>
				<?= html_writer::tag('li',
					html_writer::tag('p', $course['fullname'] . ': ' . $course['grade'], [
							'class' => 'courses-item ' . $course['class'],
							'data-id' => $key
						])
				); ?>
		<?php endforeach; ?>
	<?= html_writer::end_tag('ul') ?>
</div>

<script type="text/javascript">
	jQuery(document).ready(function() {

			<?php if($course_id): ?>
				var course_block = $(".courses li > .courses-item[data-id=<?= $course_id ?>]");
				var course_id = course_block.attr('data-id');

				if(course_block.parent('li').find('ul').length) {
					course_block.parent('li').find('ul').remove();
				} else {
					jQuery.ajax({
						url: "<?php echo $CFG->wwwroot; ?>/local/gradebook/ajax_action_courses.php",
						type: "POST",
						data: 'course_id=' + course_id,
						dataType: "html",
						beforeSend: function () {
							if(!course_block.find('i').length) {
								course_block.append('<i class="fa fa-spinner fa-spin pull-right"></i>');
							}
						}
					}).done(function (data) {
						if(course_block.parent('li').find('ul').length) {
							course_block.parent('li').find('ul').remove();
						}
						
						course_block.find('i').remove();
						course_block.parent('li').append(data);
					});
				}
			<?php endif; ?>

			$(".courses li > .courses-item").on('click', function() {
				var course_block = $(this);
				var course_id = $(this).attr('data-id');

				if(course_block.parent('li').find('ul').length) {
					course_block.parent('li').find('ul').remove();
				} else {
					jQuery.ajax({
						url: "<?php echo $CFG->wwwroot; ?>/local/gradebook/ajax_action_courses.php",
						type: "POST",
						data: 'course_id=' + course_id,
						dataType: "html",
						beforeSend: function () {
							if(!course_block.find('i').length) {
								course_block.append('<i class="fa fa-spinner fa-spin pull-right"></i>');
							}
						}
					}).done(function (data) {
						if(course_block.parent('li').find('ul').length) {
							course_block.parent('li').find('ul').remove();
						}
						course_block.find('i').remove();
						course_block.parent('li').append(data);
					});
				}
			});

			$(".courses").on('click', '.group li > .group-item', function() {
				var course_id = $(this).parents('p + ul').parent().find('.courses-item').attr('data-id');
				var group_id = $(this).attr('data-id');
				var group_block = $(this);

				if(group_block.parent('li').find('ul').length) {
					group_block.parent('li').find('ul').remove();
					group_block.find('span').html('+');
				} else {
					jQuery.ajax({
						url: "<?php echo $CFG->wwwroot; ?>/local/gradebook/ajax_action_group.php",
						type: "POST",
						data: {
							course_id: course_id,
							group_id: group_id
						},
						dataType: "html",
						beforeSend: function () {
							if(!group_block.find('i').length) {
								group_block.append('<i class="fa fa-spinner fa-spin pull-right"></i>');
							}
						}
					}).done(function (data) {
						if(group_block.find('span').html() == '+') {
							group_block.find('span').html('-');
						} else {
							group_block.find('span').html('+');
						}

						group_block.find('i').remove();

						$('.group li > .group-item').removeClass('active')
						group_block.toggleClass('active');

						group_block.parent('li').append(data);
					});
				}
			});

			$(".courses").on('click', '.categories li > .category-item', function() {
				$(this).parent('li').toggleClass('active');

				if($(this).find('span').html() == '+') {
					$(this).find('span').html('-');
				} else {
					$(this).find('span').html('+');
				}
			});

			$(".courses").on('click', '.sections li > .section-item', function() {
				$(this).parent('li').toggleClass('active');

				if($(this).find('span').html() == '+') {
					$(this).find('span').html('-');
				} else {
					$(this).find('span').html('+');
				}
			});
	});
</script>

<?php
echo $OUTPUT->footer();