<?php

class sbGradebook
{
    protected $_id;
    protected $_params;

    /**
     * Form definition.
     */
    function __construct($id = 0, $params = array())
    {
        $this->_id = $id;
        $this->_params = $params;
    }

    function rebuild($id = 0, $params = array())
    {
        $this->_id = $id;
        $this->_params = $params;
    }

    function process()
    {
        global $DB, $CFG, $USER;
        $params = (object)$this->_params;

        if ($this->_id) {
            $id = $this->_id;
            $notification = $DB->get_record('local_gradebook', array('id' => $id));
            $site = get_site();
            $supportuser = core_user::get_support_user();
            $message = new stdClass();
            $log = new stdClass();
            $log->notid = $id;
            $item = (isset($params->item)) ? $params->item : array();
            $item['support user name'] = $supportuser->firstname . ' ' . $supportuser->lastname;
            $item['support email'] = $supportuser->email;
            $item['system name'] = $site->fullname;
            $message->params = $item;
            $notification->roles = ($notification->roles != '') ? unserialize($notification->roles) : array();

            if ($notification->status > 0 and
                isset($item['userid']) and
                $item['nud_status'] == 1 and
                ($notification->courses_selection == 0 or ($notification->courses_selection > 0 and $item['nc_status'] == 1)) and
                ($notification->roles_settings == 0 or ($notification->roles_settings > 0 and !empty($item['roles']) and !empty($notification->roles) and count(array_intersect($item['roles'], $notification->roles))))
            ) {

                if ($item['n_alert'] == 1) {
                    $message->userid = $item['userid'];
                    $message->title = $this->generate_text($notification->subject, $message->params, $notification->tags);
                    $message->body = $this->generate_text($notification->body, $message->params, $notification->tags);
                    $message->notid = $notification->id;
                    $message->courseid = (isset($item['courseid'])) ? $item['courseid'] : 0;
                    $this->alert_to_user($message);
                    $log->alert = 1;
                }

                if (filter_var($item['recipient email'], FILTER_VALIDATE_EMAIL) and $item['n_email'] == 1) {
                    $message->to = $DB->get_record('user', array('id' => $item['userid']));
                    //$message->to->email = 'rubensauf@gmail.com';
                    $message->from = $supportuser;
                    $message->subject = $this->generate_text($notification->subject, $message->params, $notification->tags);
                    $message->message = $this->generate_text($notification->body, $message->params, $notification->tags);
                    email_to_user($message->to, $message->from, $message->subject, $message->message, $message->message);
                    $log->email = 1;
                }

                if ($item['n_mobile'] == 1 and $item['recipient phone'] != '' and $item['carrier'] != '') {
                    $message->userid = $item['userid'];
                    $message->phone = $item['recipient phone'];
                    $message->carrier = $item['carrier'];
                    $message->subject = $this->generate_text($notification->subject, $message->params, $notification->tags);
                    $message->body = $this->generate_text($notification->body, $message->params, $notification->tags);
                    $this->mobilemsg_to_user($message);
                    $log->mobile = 1;
                }
                $log->userid = $item['userid'];
                $log->courseid = (isset($item['courseid'])) ? $item['courseid'] : 0;
                $this->create_log($log);
            }
        }
    }

    function generate_text($text = '', $params = array(), $tags)
    {
        $message = '';
        if ($text == '') return $message;
        $tags = unserialize($tags);
        if (count($tags) > 0) {
            $message = $text;
            foreach ($tags as $tag) {
                if (strripos($message, '[' . $tag . ']') !== FALSE and isset($params[$tag])) {
                    $message = str_replace('[' . $tag . ']', $params[$tag], $message);
                }
            }
        }
        return $message;
    }

    function alert_to_user($alert = null)
    {
        global $DB;

        if ($alert) {
            $alert->new = 1;
            $alert->timecreated = time();
            $DB->insert_record('local_nots_alerts', $alert);
        }
    }

    function mobilemsg_to_user($msg = null)
    {
        global $DB;

        $msg->carrier = $this->get_carriers($msg->carrier);

        if (!empty($msg->carrier)) {
            $mail = get_mailer();
            $supportuser = core_user::get_support_user();

            $mail->Sender = $msg->params['support email'];
            $mail->From = $msg->params['support email'];
            $mail->FromName = $msg->params['support user name'];

            $mail->IsHTML(false);
            $mail->Subject = $msg->subject;
            $mail->Body = strip_tags($msg->body);

            $msg->phone = str_replace('-', '', $msg->phone);
            $msg->phone = str_replace(' ', '', $msg->phone);

            $mail->addAddress($msg->phone . $msg->carrier, '');

            $mail->send();
        }
    }

    function get_carriers($carrier = '')
    {
        $carriers = array(
            'Alltel' => '@message.alltel.com',
            'AT&T' => '@txt.att.net',
            'Boost' => '@myboostmobile.com',
            'C Spire' => '@cspire1.com',
            'CellularOne' => '@mobile.celloneusa.com',
            'Cingular' => '@cingularme.com',
            'Cricket' => '@sms.mycricket.com',
            'Nextel' => '@messaging.nextel.com',
            'Sprint PCS' => '@messaging.sprintpcs.com',
            'T-Mobile' => '@tmomail.net',
            'US Cellular' => '@email.uscc.net',
            'Verizon' => '@vtext.com',
            'Virgin Mobile' => '@vmobl.com'
        );

        if ($carrier == '') {
            return $carriers;
        }
        if (isset($carriers[$carrier])) {
            return $carriers[$carrier];
        } else {
            return '';
        }
    }

    function create_log($log = null)
    {
        global $DB;

        if ($log) {
            $log->timesend = time();
            $DB->insert_record('local_nots_logs', $log);
        }
    }
}

function get_grade_letter($sum)
{
    $letter = '';
    if ($sum >= 93) {
        $letter = "A";
    } elseif ($sum >= 90) {
        $letter = "A-";
    } elseif ($sum >= 87) {
        $letter = "B+";
    } elseif ($sum >= 83) {
        $letter = "B";
    } elseif ($sum >= 80) {
        $letter = "B-";
    } elseif ($sum >= 77) {
        $letter = "C+";
    } elseif ($sum >= 73) {
        $letter = "C";
    } elseif ($sum >= 70) {
        $letter = "C-";
    } elseif ($sum >= 67) {
        $letter = "D+";
    } elseif ($sum >= 63) {
        $letter = "D";
    } elseif ($sum >= 60) {
        $letter = "D-";
    } elseif ($sum < 60 and $sum > 0) {
        $letter = "F";
    } else {
        $letter = "-";
    }
    return $letter;
}

function pre($data, $exit = true)
{
    echo "<pre>";
        print_r($data);
    echo "</pre>";

    if($exit)
        exit();
}

function get_course_completion($course) {
    global $CFG, $USER;

    require_once("{$CFG->libdir}/completionlib.php");

    $result = new stdClass();
    $result->completion = 0;
    $result->status = 'notyetstarted';

    // Get course completion data.
    $info = new completion_info($course);

    // Load criteria to display.
    $completions = $info->get_completions($USER->id);

    if ($info->is_tracked_user($USER->id) and $course->startdate <= time()) {

        // For aggregating activity completion.
        $activities = array();
        $activities_complete = 0;

        // For aggregating course prerequisites.
        $prerequisites = array();
        $prerequisites_complete = 0;

        // Flag to set if current completion data is inconsistent with what is stored in the database.
        $pending_update = false;

        // Loop through course criteria.
        foreach ($completions as $completion) {
            $criteria = $completion->get_criteria();
            $complete = $completion->is_complete();

            if (!$pending_update && $criteria->is_pending($completion)) {
                $pending_update = true;
            }

            // Activities are a special case, so cache them and leave them till last.
            if ($criteria->criteriatype == COMPLETION_CRITERIA_TYPE_ACTIVITY) {
                $activities[$criteria->moduleinstance] = $complete;

                if ($complete) {
                    $activities_complete++;
                }

                continue;
            }

            // Prerequisites are also a special case, so cache them and leave them till last.
            if ($criteria->criteriatype == COMPLETION_CRITERIA_TYPE_COURSE) {
                $prerequisites[$criteria->courseinstance] = $complete;

                if ($complete) {
                    $prerequisites_complete++;
                }

                continue;
            }
        }

        $itemsCompleted  = $activities_complete + $prerequisites_complete;
        $itemsCount      = count($activities) + count($prerequisites);

        // Aggregate completion.
        if ($itemsCount > 0) {
            $result->completion = round(($itemsCompleted / $itemsCount) * 100);
        }

        // Is course complete?
        $coursecomplete = $info->is_course_complete($USER->id);

        // Load course completion.
        $params = array(
            'userid' => $USER->id,
            'course' => $course->id
        );
        $ccompletion = new completion_completion($params);

        // Has this user completed any criteria?
        $criteriacomplete = $info->count_course_user_data($USER->id);

        $status = '';
        if ($pending_update) {
            $status = 'pending';
        } else if ($coursecomplete) {
            $status = 'completed';
            $result->completion = 100;
        } else if ($criteriacomplete || $ccompletion->timestarted) {
            $status = 'inprogress';
        }

        $result->status = $status;
    }

    return $result;
}

function get_grade_book($course_id = null)
{
    global $CFG, $USER;

    require_once($CFG->dirroot . '/lib/grade/constants.php');
    require_once($CFG->dirroot . '/lib/grade/grade_grade.php');
    require_once($CFG->dirroot . '/lib/grade/grade_item.php');
    require_once($CFG->dirroot . '/grade/querylib.php');
    require_once($CFG->dirroot . '/lib/gradelib.php');
    require_once($CFG->dirroot . '/lib/completionlib.php');

    $user = $USER;
    $my_courses = enrol_get_my_courses('', 'fullname ASC');

    $courses = [];
    foreach ($my_courses as $key => $course) {
        if ($grade = grade_get_course_grade($user->id, $key)) {
            if ($course_id && $course_id != $key)
                continue;

            //$info = new completion_info($course);
            //$course_complete = $info->is_course_complete($USER->id);
            $completion_info = get_course_completion($course);
            $grade = 'Not started';
            if($completion_info->status) {
                $grade = ($completion_info->status == 'notyetstarted' || $completion_info->status == "pending" || $completion_info->status == 'inprogress') ? 'Incomplete' : 'Complete';
            }

            $courses[$key] = [
                'id' => $key,
                'fullname' => $course->fullname,
                'grade' =>  $grade,
                'class' => 'course-status ' . $completion_info->status,
            ];
        }
    }
    return $courses;
}

function get_course_group($id)
{

    global $CFG, $DB, $PAGE;
    
    $PAGE->set_context(context_system::instance());

    require_once(dirname(__FILE__) . '/../../config.php');
    require_once($CFG->libdir . '/gradelib.php');
    require_once($CFG->dirroot . '/mod/quiz/locallib.php');
    require_once($CFG->libdir . '/completionlib.php');
    require_once($CFG->dirroot . '/course/format/lib.php');
    require_once($CFG->dirroot . '/course/lib.php');
    require_once($CFG->dirroot . '/lib/modinfolib.php');

    $sections = [];
    $modinfo = get_fast_modinfo($id);

    $course = course_get_format($id)->get_course();
    //$course = $DB->get_record('course', array('id' => $id));
    $format_options = $DB->get_record('course_format_options', array('courseid' => $id, 'format' => $course->format, 'name' => 'numsections'));

    $format_renderer = $PAGE->get_renderer('format_institutes');
    foreach ($modinfo->get_section_info_all() as $section => $thissection) {

        $progress = $format_renderer->get_section_completion($course, $thissection->section);

        if ($section == 0)
            continue;

        if ($section > $format_options->value)
            break;

        $showsection = $thissection->uservisible || ($thissection->visible && !$thissection->available && !empty($thissection->availableinfo));
        if (!$showsection)
            continue;

        if (intval($thissection->parent) == 0) {
            $sections['root'][$thissection->section]['obj'] = $thissection;
            $sections['root'][$thissection->section]['class'] = $progress['status'];
        }
    }
    //pre($sections['root']);
    return $sections;
}

function get_course_sub_group($id, $section_id)
{

    global $CFG, $PAGE;
    
    $PAGE->set_context(context_system::instance());

    require_once(dirname(__FILE__) . '/../../config.php');
    require_once($CFG->libdir . '/gradelib.php');
    require_once($CFG->dirroot . '/mod/quiz/locallib.php');
    require_once($CFG->libdir . '/completionlib.php');
    require_once($CFG->dirroot . '/course/format/lib.php');
    require_once($CFG->dirroot . '/course/lib.php');
    require_once($CFG->dirroot . '/lib/modinfolib.php');

    $sections = [];
    $modinfo = get_fast_modinfo($id);

    //$course = course_get_format($id)->get_course();
    foreach ($modinfo->get_section_info_all() as $section => $thissection) {
        if ($section == 0)
            continue;

        /*if ($section > $course->numsections)
            break;*/

        $showsection = $thissection->uservisible || ($thissection->visible && !$thissection->available && !empty($thissection->availableinfo));
        if (!$showsection)
            continue;

        if (intval($thissection->parent) > 0) {
            $sections[$thissection->parent][$thissection->section] = $thissection;
        } else {
            $sections['root'][$thissection->section] = $thissection;
        }
    }

    $sub_sections = [];
    if (isset($sections[$section_id]) && $sections[$section_id]) {
        foreach ($sections[$section_id] as $section => $thissection) {
            $sub_sections[$section] = $thissection;
        }
    }
    return $sub_sections;
}

function get_course_sections($course_id)
{
    global $DB;

    $sections = [];
    $section_records = $DB->get_records('course_sections', array('course' => $course_id));
    if (count($section_records)) {
        foreach ($section_records as $section) {
            $sections[$section->id] = $section;
        }
    }
    return $sections;
}

function get_quiz_list($id, $section_id)
{
    global $CFG, $USER, $DB;

    require_once(dirname(__FILE__) . '/../../config.php');
    require_once($CFG->libdir . '/gradelib.php');
    require_once($CFG->dirroot . '/mod/quiz/locallib.php');
    require_once($CFG->libdir . '/completionlib.php');
    require_once($CFG->dirroot . '/course/format/lib.php');
    require_once($CFG->dirroot . '/course/lib.php');
    require_once($CFG->dirroot . '/lib/modinfolib.php');

    $list = [];

    if($sections = get_course_sub_group($id, $section_id)) {
        foreach ($sections as $section) {
            $records_course_modules = $DB->get_records_sql("SELECT id FROM mdl_course_modules where course = :id AND section = :section_id", [
                'id' => $id,
                'section_id' => $section->id
            ]);

            foreach ($records_course_modules as $record_cm) {

                if (!$cm = get_coursemodule_from_id('quiz', $record_cm->id))
                    continue;

                $course = course_get_format($cm->course)->get_course();
                if (!$course)
                    continue;

                $quizobj = quiz::create($cm->instance, $USER->id);
                if ($quiz = $quizobj->get_quiz()) {

                    $line = [
                        'name' => $quiz->name,
                        'state' => '-',
                        'time_start' => null,
                        'time_modified' => null,
                        'time_finish' => null,
                        'time_taken' => null,
                        'attempt' => null,
                        'avg_grade' => '-/-',
                    ];


                    if ($records_attempt = $DB->get_record_sql("SELECT state, timestart, timemodified, timefinish, attempt FROM mdl_quiz_attempts where quiz = :id AND userid=:user_id", [
                        'id' => $quiz->id,
                        'user_id' => $USER->id
                    ])
                    ) {
                        $line['state'] = $records_attempt->state;
                        $line['time_start'] = $records_attempt->timestart;
                        $line['time_modified'] = $records_attempt->timemodified;
                        $line['time_finish'] = $records_attempt->timefinish;
                        $line['attempt'] = $records_attempt->attempt;

                        if ($records_attempt->timestart && $records_attempt->timefinish) {
                            $line['time_taken'] = $records_attempt->timefinish - $records_attempt->timestart;
                        }

                        $record_grade = $DB->get_record_sql('SELECT g.rawgrademax, g.rawgrademin, g.finalgrade FROM mdl_grade_items as i LEFT JOIN mdl_grade_grades as g ON (g.userid=:userid AND g.itemid=i.id) where (i.iteminstance=:iteminstance AND i.itemtype=:itemtype AND i.itemmodule=:itemmodule)', [
                            'iteminstance' => $quiz->id,
                            'itemtype' => 'mod',
                            'itemmodule' => 'quiz',
                            'userid' => $USER->id
                        ]);

                        if ($record_grade->finalgrade) {
                            $progress = round((floatval($record_grade->finalgrade) / floatval($record_grade->rawgrademax)) * 100);
                            $line['avg_grade'] = get_grade_letter($progress) . '/' . $progress . '%';
                        }
                    }
                    $list[] = $line;
                }
            }
        }
    }
    return $list;
}

?>

