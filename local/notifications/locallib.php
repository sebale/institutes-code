<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Notifications version file.
 *
 * @package    local_notifications
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

/*class events_handler {

    public static function course_completed($ue) {
        global $DB, $CFG;

        require_once($CFG->dirroot . '/local/notifications/lib.php');
        $params = array(); $item = array();

        $user = $DB->get_record("user", array('id' => $ue->userid));
        $course = $DB->get_record("course", array('id' => $ue->courseid));
        $context = context_course::instance($course->id, IGNORE_MISSING);
        $roles = $DB->get_records('role_assignments', array('contextid'=>$context->id, 'userid'=>$user->id));
        $user_roles = array();
        if (count($roles)){
            foreach($roles as $role){
                $user_roles[$role->roleid] = $role->roleid;
            }
        }

        $item['userid'] = $user->id;
        $item['recipient name'] = fullname($user);
        $item['recipient email'] = $user->email;
        $item['recipient phone'] = $user->phone2;
        $item['roles'] = $user_roles;
        $item['course name'] = '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'">'.$course->fullname.'</a>';
        $item['courseid'] = $course->id;

        $not_id = 7;

        $not_settings = $DB->get_record_sql("SELECT u.*, nud.alert as n_alert, nud.email as n_email, nud.mobile as n_mobile, nud.status as nud_status, uc.carrier as carrier, n.status as not_status 
                        FROM {user} as u
                            LEFT JOIN {local_notifications} n ON n.id = $not_id
                            LEFT JOIN {local_nots_userdata} as nud ON nud.userid = u.id AND nud.notid = n.id
                            LEFT JOIN (SELECT uid.userid, uid.data as carrier FROM {user_info_data} uid LEFT JOIN {user_info_field} uif ON uif.id = uid.fieldid WHERE uif.shortname = 'carrier') as uc ON uc.userid = u.id
                                WHERE u.id = $user->id");
    }


    public static function mod_created($ue) {
        global $DB, $CFG;

        require_once($CFG->dirroot . '/local/notifications/lib.php');
        $params = array(); $item = array();

        $user = $DB->get_record("user", array('id' => $ue->userid));
        $course = $DB->get_record("course", array('id' => $ue->courseid));
        $context = context_course::instance($course->id, IGNORE_MISSING);
        $roles = $DB->get_records('role_assignments', array('contextid'=>$context->id, 'userid'=>$user->id));
        $user_roles = array();
        if (count($roles)){
            foreach($roles as $role){
                $user_roles[$role->roleid] = $role->roleid;
            }
        }

        $item['userid'] = $user->id;
        $item['recipient name'] = fullname($user);
        $item['recipient email'] = $user->email;
        $item['recipient phone'] = $user->phone2;
        $item['roles'] = $user_roles;
        $item['course name'] = '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'">'.$course->fullname.'</a>';
        $item['courseid'] = $course->id;

        $not_id = 12;

        $not_settings = $DB->get_record_sql("SELECT u.*, nud.alert as n_alert, nud.email as n_email, nud.mobile as n_mobile, nud.status as nud_status, uc.carrier as carrier, n.status as not_status 
                        FROM {user} as u
                            LEFT JOIN {local_notifications} n ON n.id = $not_id
                            LEFT JOIN {local_nots_userdata} as nud ON nud.userid = u.id AND nud.notid = n.id
                            LEFT JOIN (SELECT uid.userid, uid.data as carrier FROM {user_info_data} uid LEFT JOIN {user_info_field} uif ON uif.id = uid.fieldid WHERE uif.shortname = 'carrier') as uc ON uc.userid = u.id
                                WHERE u.id = $user->id");
    }

    public static function mod_deleted($ue) {
        global $DB, $CFG;

        require_once($CFG->dirroot . '/local/notifications/lib.php');
        $params = array(); $item = array();

        $user = $DB->get_record("user", array('id' => $ue->userid));
        $course = $DB->get_record("course", array('id' => $ue->courseid));
        $context = context_course::instance($course->id, IGNORE_MISSING);
        $roles = $DB->get_records('role_assignments', array('contextid'=>$context->id, 'userid'=>$user->id));
        $user_roles = array();
        if (count($roles)){
            foreach($roles as $role){
                $user_roles[$role->roleid] = $role->roleid;
            }
        }

        $item['userid'] = $user->id;
        $item['recipient name'] = fullname($user);
        $item['recipient email'] = $user->email;
        $item['recipient phone'] = $user->phone2;
        $item['roles'] = $user_roles;
        $item['course name'] = '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'">'.$course->fullname.'</a>';
        $item['courseid'] = $course->id;

        $not_id = 14;

        $not_settings = $DB->get_record_sql("SELECT u.*, nud.alert as n_alert, nud.email as n_email, nud.mobile as n_mobile, nud.status as nud_status, uc.carrier as carrier, n.status as not_status 
                        FROM {user} as u
                            LEFT JOIN {local_notifications} n ON n.id = $not_id
                            LEFT JOIN {local_nots_userdata} as nud ON nud.userid = u.id AND nud.notid = n.id
                            LEFT JOIN (SELECT uid.userid, uid.data as carrier FROM {user_info_data} uid LEFT JOIN {user_info_field} uif ON uif.id = uid.fieldid WHERE uif.shortname = 'carrier') as uc ON uc.userid = u.id
                                WHERE u.id = $user->id");
    }

    public static function mod_updated($ue) {
        global $DB, $CFG;

        require_once($CFG->dirroot . '/local/notifications/lib.php');
        $params = array(); $item = array();

        $user = $DB->get_record("user", array('id' => $ue->userid));
        $course = $DB->get_record("course", array('id' => $ue->courseid));
        $context = context_course::instance($course->id, IGNORE_MISSING);
        $roles = $DB->get_records('role_assignments', array('contextid'=>$context->id, 'userid'=>$user->id));
        $user_roles = array();
        if (count($roles)){
            foreach($roles as $role){
                $user_roles[$role->roleid] = $role->roleid;
            }
        }

        $item['userid'] = $user->id;
        $item['recipient name'] = fullname($user);
        $item['recipient email'] = $user->email;
        $item['recipient phone'] = $user->phone2;
        $item['roles'] = $user_roles;
        $item['course name'] = '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'">'.$course->fullname.'</a>';
        $item['courseid'] = $course->id;

        $not_id = 13;

        $not_settings = $DB->get_record_sql("SELECT u.*, nud.alert as n_alert, nud.email as n_email, nud.mobile as n_mobile, nud.status as nud_status, uc.carrier as carrier, n.status as not_status 
                        FROM {user} as u
                            LEFT JOIN {local_notifications} n ON n.id = $not_id
                            LEFT JOIN {local_nots_userdata} as nud ON nud.userid = u.id AND nud.notid = n.id
                            LEFT JOIN (SELECT uid.userid, uid.data as carrier FROM {user_info_data} uid LEFT JOIN {user_info_field} uif ON uif.id = uid.fieldid WHERE uif.shortname = 'carrier') as uc ON uc.userid = u.id
                                WHERE u.id = $user->id");
    }

    public static function user_enrolled($ue) {
        global $DB, $CFG;

        /* 
        $alert = new stdClass();

        $alert->userid = 123;
        $alert->title = 'title bla';
        $alert->body = 'bla bla';
        $alert->notid = 1;
        $alert->courseid = 1;
        $alert->new = 1;
        $alert->timecreated = time();
        $DB->insert_record('local_nots_alerts', $alert);
        */
        
        /*require_once($CFG->dirroot . '/local/notifications/lib.php');
        $params = array(); $item = array();

        $user = $DB->get_record("user", array('id' => $ue->userid));
        $course = $DB->get_record("course", array('id' => $ue->courseid));
        $context = context_course::instance($course->id, IGNORE_MISSING);
        $roles = $DB->get_records('role_assignments', array('contextid'=>$context->id, 'userid'=>$user->id));
        $user_roles = array();
        if (count($roles)){
            foreach($roles as $role){
                $user_roles[$role->roleid] = $role->roleid;
            }
        }
        
        $item['userid'] = $user->id;
        $item['recipient name'] = fullname($user);
        $item['recipient email'] = $user->email;
        $item['recipient phone'] = $user->phone2;
        $item['roles'] = $user_roles;
        $item['course name'] = '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'">'.$course->fullname.'</a>';
        $item['courseid'] = $course->id;

        $not_id = 1;
        
        $not_settings = $DB->get_record_sql("SELECT u.*, nud.alert as n_alert, nud.email as n_email, nud.mobile as n_mobile, nud.status as nud_status, uc.carrier as carrier, n.status as not_status 
                        FROM {user} as u
                            LEFT JOIN {local_notifications} n ON n.id = $not_id
                            LEFT JOIN {local_nots_userdata} as nud ON nud.userid = u.id AND nud.notid = n.id
                            LEFT JOIN (SELECT uid.userid, uid.data as carrier FROM {user_info_data} uid LEFT JOIN {user_info_field} uif ON uif.id = uid.fieldid WHERE uif.shortname = 'carrier') as uc ON uc.userid = u.id
                                WHERE u.id = $user->id");
        if ($not_settings->not_status > 0){
            $item['nud_status'] = (isset($not_settings->nud_status)) ? $not_settings->nud_status : 0;
            $item['n_alert'] = 1; //(isset($not_settings->n_alert)) ? $not_settings->n_alert : 0;
            $item['n_email'] = 1; //(isset($not_settings->n_email)) ? $not_settings->n_email : 0;
            //$item['n_mobile'] = (isset($not_settings->n_mobile)) ? $not_settings->n_mobile : 0;
            $item['carrier'] = (isset($not_settings->carrier)) ? $not_settings->carrier : '';
            $params['item'] = $item;
            $msg = new sbNotifications($not_id, $params);
            $msg->process();
        }
       
        return true;
    }
    
    public static function user_unenrolled($ue){
        global $DB, $CFG;
        
        require_once($CFG->dirroot.'/local/notifications/lib.php');
        $params = array(); $item = array();

        $user = $DB->get_record("user", array('id' => $ue->userid));
        $course = $DB->get_record("course", array('id' => $ue->courseid));
        $context = context_course::instance($course->id, IGNORE_MISSING);
        $roles = $DB->get_records('role_assignments', array('contextid'=>$context->id, 'userid'=>$user->id));
        $user_roles = array();
        if (count($roles)){
            foreach($roles as $role){
                $user_roles[$role->roleid] = $role->roleid;
            }
        }
        $item['userid'] = $user->id;
        $item['recipient name'] = fullname($user);
        $item['recipient email'] = $user->email;
        $item['recipient phone'] = $user->phone2;
        $item['roles'] = $user_roles;
        $item['course name'] = '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'">'.$course->fullname.'</a>';
        $item['courseid'] = $course->id;
        
        $not_id = 4;
        $not_settings = $DB->get_record_sql("SELECT u.id, nud.alert as n_alert, nud.email as n_email, nud.mobile as n_mobile, nud.status as nud_status, uc.carrier as carrier, n.status as not_status
                FROM {user} as u
                    LEFT JOIN {local_notifications} n ON n.id = $not_id
                    LEFT JOIN {local_nots_userdata} as nud ON nud.userid = u.id AND nud.notid = n.id
                    LEFT JOIN (SELECT uid.userid, uid.data as carrier FROM {user_info_data} uid LEFT JOIN {user_info_field} uif ON uif.id = uid.fieldid WHERE uif.shortname = 'carrier') as uc ON uc.userid = u.id
                        WHERE u.id = $user->id");
        if ($not_settings->not_status > 0){
            $item['nud_status'] = (isset($not_settings->nud_status)) ? $not_settings->nud_status : 0;
            $item['n_alert'] = (isset($not_settings->n_alert)) ? $not_settings->n_alert : 0;
            $item['n_email'] = (isset($not_settings->n_email)) ? $not_settings->n_email : 0;
            //$item['n_mobile'] = (isset($not_settings->n_mobile)) ? $not_settings->n_mobile : 0;
            //$item['carrier'] = (isset($not_settings->carrier)) ? $not_settings->carrier : '';
            //$item['carrier'] = (isset($not_settings->carrier)) ? $not_settings->carrier : '';
            $params['item'] = $item;
            $msg = new sbNotifications($not_id, $params);
            $msg->process();
        }
        
        return true;
    } 
}*/
