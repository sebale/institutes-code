<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * TalentQuest version file.
 *
 * @package    local_notifications
 * @author     TalentQuest
 * @copyright  2016 talentquest.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die;

if (!$ADMIN->locate('local_notifications') and $ADMIN->locate('localplugins')){
    $settings = new admin_settingpage('local_notifications', get_string('pluginname', 'local_notifications'));
    $ADMIN->add('localplugins', $settings);
    
    $ADMIN->add('root', new admin_externalpage('sb_notifications', get_string('pluginname', 'local_notifications'), $CFG->wwwroot.'/local/notifications/notifications.php', 'local/notifications:view'));
}


