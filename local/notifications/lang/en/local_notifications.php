<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * TalentQuest version file.
 *
 * @package    local_notifications
 * @author     TalentQuest
 * @copyright  2016 talentquest.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

$string['pluginname'] = 'Notifications';
$string['settings'] = 'Settings';
$string['notifications_settings'] = 'Notifications Settings';
$string['notifications:manage'] = 'Notifications manage';
$string['notifications:view'] = 'Notifications view';


