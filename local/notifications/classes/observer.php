<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local stuff for category enrolment plugin.
 *
 * @package    local_notifications
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Event observer for transcripts.
 */
class local_notifications_observer {

    /**
     * id - from table [local_notifications]
     * const event = id
     */
    const EVENT_USER_ENROLMENT_CREATE = 1;
    const EVENT_USER_ENROLMENT_UPDATE = 2;
    const EVENT_USER_ENROLMENT_DELETE = 3;
    const EVENT_MODULE_COURSE_CREATE = 4;
    const EVENT_MODULE_COURSE_UPDATE = 5;
    const EVENT_MODULE_COURSE_DELETE = 6;

    /**
     * Prepare data trigger event
     *
     * @param $event - moodle \core\event\...
     * @param $template_id - [id] from table [local_notifications]
     * @param array $params - additional params in notification message
     * @return bool
     */
    static function _notification($template_id, $event, $params = []) {
        global $DB, $CFG;

        require_once($CFG->dirroot . '/local/notifications/lib.php');

        $user_id = (isset($params['enroled']) && $params['enroled']) ? $event->relateduserid : $event->userid;
        $user = $DB->get_record("user", array('id' => $user_id));

        $course = $DB->get_record("course", array('id' => $event->courseid));
        $context = context_course::instance($course->id, IGNORE_MISSING);
        $user_roles = array();
        if ($roles = $DB->get_records('role_assignments', [
                'contextid' => $context->id,
                'userid' => $user->id
        ])) {
            foreach($roles as $role){
                $user_roles[$role->roleid] = $role->roleid;
            }
        }

        if(!isset($params['item'])) {
            $params['item'] = [
                'userid' => $user->id,
                'recipient name' => fullname($user),
                'recipient email' => $user->email,
                'recipient phone' => $user->phone2,
                'roles' => $user_roles,
                'course name' => '<a href="' . $CFG->wwwroot . '/course/view.php?id=' . $course->id . '">' . $course->fullname . '</a>',
                'courseid' => $course->id,
                'n_alert' => true,
                'n_email' => true
            ];
        }
        $notification = new sbNotifications($template_id, $params);
        return $notification->process();
    }

    public static function user_enrolment_created(\core\event\user_enrolment_created $event) {
        return self::_notification(self::EVENT_USER_ENROLMENT_CREATE, $event, ['enroled' => true]);
    }

    public static function user_enrolment_updated(\core\event\user_enrolment_updated $event) {
        return self::_notification(self::EVENT_USER_ENROLMENT_UPDATE, $event, ['enroled' => true]);
    }

    public static function user_enrolment_deleted(\core\event\user_enrolment_deleted $event) {
        return self::_notification(self::EVENT_USER_ENROLMENT_DELETE, $event, ['enroled' => true]);
    }

    public static function course_module_created(\core\event\course_module_created $event) {
        return self::_notification(self::EVENT_MODULE_COURSE_CREATE, $event);
    }

    public static function course_module_updated(\core\event\course_module_updated $event) {
        return self::_notification(self::EVENT_MODULE_COURSE_UPDATE, $event);
    }

    public static function course_module_deleted(\core\event\course_module_deleted $event) {
        return self::_notification(self::EVENT_MODULE_COURSE_DELETE, $event);
    }
}
